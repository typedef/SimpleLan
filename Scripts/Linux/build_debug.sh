#!/bin/bash

cd ../..

outputDir="Bin"
sources=$(find ./SimpleLan -name "*.c")
appName="SimpleLAN"

# Creating main output dir
if [ ! -d $outputDir ]
then
    mkdir $outputDir
fi

echo $(pwd)

gcc $sources -o $outputDir/$appName DepsBin/libEngine.a -DLINUX_PLATFORM -IDepsInclude/
$outputDir/$appName
