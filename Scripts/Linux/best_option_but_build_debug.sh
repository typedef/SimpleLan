#!/bin/bash

outputDir="Bin"
projectSrc="SimpleLan/src"
interDirName="Intermediates"
interDir=$outputDir/$interDirName

cd ../..

simpleLibDepsInclude="Dependencies/SimpleLib/SimpleLib/src/"
simpleLibDepsSources=$(find ./Dependencies -name "*.c")
simpleLibName="SimpleLib"

# Creating main output dir
if [ ! -d $outputDir ]
then
    mkdir $outputDir
fi

cd Dependencies/$simpleLibName

# Creating bin/inter/ dirs
cd
if [ ! -d $outputDir ]
then
    mkdir $outputDir
fi
if [ ! -d $interDir ]
then
    mkdir $interDir
fi

for depSource in ${simpleLibDepsSources[*]}
do
    objectName=$($depSource | sed -r "s/.+\/(.+)\..+/\1/")
    gcc -c -o $interDir/$objectName $depSource -DLINUX_PLATFORM
done

# building deps
#gcc -c -o $interDir/ $simpleLibDepsSources
#ar rcs libout.a out.o

#gcc $projectSrc/main.c -o $outputDir/App -I$depsInclude
#$outputDir/App
