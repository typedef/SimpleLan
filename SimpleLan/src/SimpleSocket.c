#include "SimpleSocket.h"

#include <stdio.h>
#include <Utils/SimpleStandardLibrary.h>

//NOTE(bies): linux socket stuff
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h> // for close
#include <netinet/in.h>
#include <fcntl.h>

i32 socket_type_get(ConnectionProtocolType type);
i32 linux_ip_type_from_ip_type(IpType type);
struct sockaddr_in connection_address(IpType type, u16 port, u32 ip);
void connection_address_to_string(char str[], struct sockaddr_in address);

u32
ip_as_integer(u8 ip3, u8 ip2, u8 ip1, u8 ip0)
{
    return ip3 << 24 | ip2 << 16 | ip1 << 8 | ip0;
}

void
ip_as_string(char str[], u32 ip)
{
    u8* ptr = (u8*)&ip;
    u8 byte3 = *(ptr+3);
    u8 byte2 = *(ptr+2);
    u8 byte1 = *(ptr+1);
    u8 byte0 = *(ptr);
    sprintf(str, "%d.%d.%d.%d", byte3, byte2, byte1, byte0);
}

void
ip_print(u32 address)
{
    char ips[32] = {};
    ip_as_string(ips, address);
    printf("ip=%s\n", ips);
}

char*
ip_to_string(u32 address)
{
    static char ips[32] = {};
    ip_as_string(ips, address);
    return (char*) ips;
}

u32
htonf(f32 f)
{
    u32 p;
    u32 sign;

    if (f < 0)
    {
	sign = 1;
	f = -f;
    }
    else
    {
	sign = 0;
    }

    p  = ((((u32)f) & 0x7fff) <<16) | (sign << 31); // whole part and sign
    p |= (u32) (((f - (i32)f) * 65536.0f)) & 0xffff; // fraction

    return p;
}

f32
ntohf(u32 p)
{
    f32 f = ((p >> 16) & 0x7fff); // whole part
    f += (p & 0xffff) / 65536.0f; // fraction

    if (((p >> 31) & 0x1) == 0x1)
    {
	f = -f; // sign bit set
    }

    return f;
}

// Localhost: INADDR_LOOPBACK
SimpleSocket
simple_socket_tcp_create(SimpleSocketSetting set)
{
    SimpleSocket simpleSocket = {
	.Address = set.Address,
	.Port = set.Port
    };

    i32 socketType = socket_type_get(ConnectionProtocolType_TCP);

    i32 socketFd = socket(AF_INET, socketType, 0);
    if (!socketFd)
    {
	GERROR("Can't create socket descriptor!\n");
	close(socketFd);
	vassert_break();
    }

    if (set.IsAsync)
    {
	fcntl(socketFd, F_SETFL, O_NONBLOCK);
    }

    simpleSocket.Descriptor = socketFd;

    struct sockaddr_in conAddress = connection_address(IpType_V4, set.Port, set.Address);
    if (set.IsServer)
    {
	if (bind(socketFd, (struct sockaddr*) &conAddress,  sizeof(struct sockaddr_in)) > 0)
	{
	    vguard(0 && "Can't bind address");
	}

	if (listen(socketFd, set.MaxClients) < 0)
	{
	    vguard(0 && "Can't listen on server!");
	}
    }
    else
    {
	if (connect(socketFd, (struct sockaddr*) &conAddress, sizeof(conAddress)) < 0)
	{
	    vassert(0 && "Connection error!");
	}
    }

    return simpleSocket;
}

SimpleSocket
simple_socket_udp_create(SimpleSocketSetting set)
{
    SimpleSocket simpleSocket = {
	.Address = set.Address,
	.Port = set.Port
    };

    i32 socketType = socket_type_get(ConnectionProtocolType_UDP);

    i32 socketFd = socket(AF_INET, socketType, 0);
    struct sockaddr_in conAddress = connection_address(IpType_V4, set.Port, set.Address);
    if (!socketFd)
    {
	GERROR("Can't create socket descriptor!\n");
	close(socketFd);
	vassert_break();
    }

    if (bind(socketFd, (struct sockaddr*) &conAddress,  sizeof(struct sockaddr_in)) > 0)
    {
	vguard(0 && "Can't bind address");
    }

    simpleSocket.Descriptor = socketFd;

    return simpleSocket;
}

i32
simple_socket_connect(SimpleSocket socket, u32 address, u16 port)
{
    struct sockaddr_in conAddress = connection_address(IpType_V4, port, address);
    if (connect(socket.Descriptor, (struct sockaddr*) &conAddress, sizeof(struct sockaddr_in)) < 0)
    {
	vassert(0 && "Connection error!");
	return 0;
    }

    return 1;
}

u32
simple_socket_get_ip_address(SimpleSocket socket)
{
    i32 len = sizeof(struct sockaddr_in);
    struct sockaddr_in addr;
    getpeername(socket.Descriptor, (struct sockaddr*)&addr, &len);

    return addr.sin_addr.s_addr;
}
void
simple_socket_print_ip(SimpleSocket socket)
{
    u32 addr = simple_socket_get_ip_address(socket);
    char ips[32] = {};
    ip_as_string(ips, addr);
    printf("ip=%s port=%d\n", ips);
}


i32
simple_socket_send_ext(SimpleSocket socket, void* data, size_t size, i32 flags)
{
    i32 sendedBytesCount, total = 0;

    while (total < size)
    {
	i32 byteToSend = size - total;
	sendedBytesCount = send(socket.Descriptor, data + total, byteToSend, flags);

	if (sendedBytesCount == -1)
	{
	    return 0;
	}

	total += sendedBytesCount;
    }

    return 1;
}

i32
simple_socket_send(SimpleSocket socket, void* data, size_t size)
{
    return simple_socket_send_ext(socket, data, size, 0);
}

i32
simple_socket_recv_ext(SimpleSocket socket, void* data, size_t size, i32 flags)
{
    i32 recvBytesCount, total = 0;
    while (total < size)
    {
	i32 byteToRecv = size - total;
	recvBytesCount = recv(socket.Descriptor, data + total, byteToRecv, flags);

	if (recvBytesCount == -1 || recvBytesCount == 0)
	{
	    return 0;
	}

	total += recvBytesCount;
    }

    return 1;
}

i32
simple_socket_recv(SimpleSocket socket, void* data, size_t size)
{
    return simple_socket_recv_ext(socket, data, size, 0);
}

i32
simple_socket_send_to_ext(SimpleSocket from, struct sockaddr_in* to , void* data, i32 size, u32 flags)
{
    i32 sendedBytesCount, total = 0;

    while (total < size)
    {
	i32 byteToSend = size - total;
	sendedBytesCount = sendto(from.Descriptor, data + total, byteToSend, flags, (const struct sockaddr*)to, byteToSend);

	if (sendedBytesCount == -1)
	{
	    return 0;
	}

	total += sendedBytesCount;
    }

    return 1;

}

i32
simple_socket_send_to(SimpleSocket from, SimpleSocket to, void* data, size_t size)
{
    struct sockaddr_in address = connection_address(IpType_V4, to.Port, to.Address);
    return simple_socket_send_to_ext(from, &address , data, size, 0);
}

i32
simple_socket_recv_from_ext(SimpleSocket to, struct sockaddr_in* from, void* data, i32 size, u32 flags)
{
    i32 sendedBytesCount, total = 0, fromSize = sizeof(struct sockaddr);

    while (total < size)
    {
	i32 byteToSend = size - total;
	sendedBytesCount = recvfrom(to.Descriptor, data + total, byteToSend, flags, (struct sockaddr*) from, &fromSize);

	if (sendedBytesCount == -1)
	{
	    return 0;
	}

	total += sendedBytesCount;
    }

    return 1;

}

i32
simple_socket_recv_from(SimpleSocket to, SimpleSocket from, void* data, i32 size)
{
    struct sockaddr_in address = connection_address(IpType_V4, from.Port, from.Address);
    return simple_socket_recv_from_ext(to, &address, data, size, 0);
}

SimpleSocket
simple_socket_accept(SimpleSocket socket)
{
    i32 descriptor = accept(socket.Descriptor, NULL,NULL);
    if (descriptor == -1)
    {
	GWARNING("Wrong descriptor %d\n", descriptor);
    }

    SimpleSocket sSocket = {
	.Descriptor = descriptor
    };

    return sSocket;
}

void
simple_socket_bind(SimpleSocket socket, u32 address, u16 port)
{
    struct sockaddr_in conAddress = connection_address(IpType_V4, port, address);
    if (bind(socket.Descriptor, (struct sockaddr*) &conAddress,  sizeof(struct sockaddr_in)) > 0)
    {
	vguard(0 && "Can't bind socket!");
    }
}

void
simple_socket_make_async(SimpleSocket* pSocket)
{
    fcntl(pSocket->Descriptor, F_SETFL, O_NONBLOCK);
    pSocket->IsAsync = 1;
}

i32
simple_socket_close(SimpleSocket socket)
{
    return close(socket.Descriptor);
}

void
simple_socket_print(SimpleSocket socket)
{
    char ipAsStr[32] = {};
    ip_as_string(ipAsStr, socket.Address);
    printf("Socket: %s:%d\n", ipAsStr, socket.Port);
}

i32
socket_type_get(ConnectionProtocolType type)
{
    switch (type)
    {
    case ConnectionProtocolType_TCP: return SOCK_STREAM;
    case ConnectionProtocolType_UDP: return SOCK_DGRAM;
    }

    vassert(0 && "Wrong ConnectionProtocolType!");
    return 0;
}

struct sockaddr_in
connection_address(IpType type, u16 port, u32 ip)
{
    struct sockaddr_in addr = {
	.sin_family = AF_INET,
	.sin_addr.s_addr = htonl(ip),
	.sin_port = htons(port),
    };

    return addr;
}

void
connection_address_to_string(char str[], struct sockaddr_in address)
{
    ip_as_string(str, address.sin_addr.s_addr);
}


void
sockets_test()
{
    u32 ip = ip_as_integer(10, 8, 0, 3);
    char ipAsString[256];
    ip_as_string(ipAsString, ip);
    GLOG("Ip: %s\n", ipAsString);
}

#if 0
void
server_nonblocking()
{
    GINFO("Runing server ...\n");
    i32 socketType = socket_type_get(UDP);
    i32 serverSocket = socket(linux_ip_type_from_ip_type(IpType_V4), socketType, 0);
    if (!serverSocket)
    {
	close(serverSocket);
	GERROR("Can't create listen socket descriptor!");
	return -1;
    }

    fcntl(serverSocket, F_SETFL, 0_NONBLOCK);

    //NOTE(bies): используем sockaddr_in вместо sockaddr
    struct sockaddr_in connectionAddress = connection_address(IpType_V4, 3904, INADDR_ANY);
    if (bind(serverSocket, (struct sockaddr*)&connectionAddress, sizeof(connectionAddress)) < 0)
    {
	vassert(0 && "bind error!");
    }

    const i32 maxSocketsCount = 3;
    listen(serverSocket, maxSocketsCount);

    i32* clients = NULL;
    array_reserve(clients, maxSocketsCount);

    timeval timeout;
    timeout.tv_sec  = 15;
    timeout.tv_usec = 0;

    i32 clientSocket;
    while (1)
    {
	fd_set readset;
	FD_ZERO(&readset);
	FD_SET(serverSocket, &readset);
	array_foreach(clients, FD_SET(item, &readset););

	if (select(maxSocketsCount + 1, &readset, NULL, NULL, &timeout) <= 0)
	{
	    vassert(0 && "Can't select()");
	}

	i32 i, count = array_count(clients);
	for (i = 0; i < count; ++i)
	{
	    i32 client = clients[i];
	    if (FD_ISSET(client, &readset))
	    {
		char buf[256];
		buf[255] = '\0';
		struct sockaddr_in clientAddress;
		i32 clientRecvLength;

		i32 bytesRead = recvfrom(client, buf, sizeof(buf), 0, &clientAddress, &clientRecvLength);
		if (bytesRead <= 0)
		{
		    close(item);
		    array_remove_at(clients, i);
		}

		char clientIpStr[256];
		ip_as_string(clientIpStr, ntohl(clientAddress.sin_addr.s_addr));
		GLOG("Recv message: %s [length: %d ip: %s]\n", buf, messageLength, clientIpStr);
		char message[] = "Server response";
		sendto(serverSocket, message, sizeof(message), 0, &clientAddress, sizeof(clientAddress));
	    }
	}
    }

    close(serverSocket);
}
#endif
