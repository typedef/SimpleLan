#ifndef SIMPLE_TAP_H
#define SIMPLE_TAP_H

#include <Utils/Types.h>

typedef struct SimpleTapSettings
{
    char* PreferredTapName;
    u32 IpAddress;
    u8 MaskBits;
} SimpleTapSettings;

typedef struct SimpleTap
{
    char* DeviceName;
    i32 Mtu;
    i32 Mss;
    i32 Fd;
    u8 MacAsBytes[6];
    u32 VlanIp4;
} SimpleTap;

SimpleTap tap_create(SimpleTapSettings set);
void tap_connect(SimpleTap* pSimpleTap, i32 connect);

#endif // TAP_H
