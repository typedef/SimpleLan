#include <Utils/SimpleThread.h>
#include <Utils/SimpleStandardLibrary.h>

#include "SimpleSocket.h"
#include "SimpleLan.h"
#include "SimpleTap.h"
#include <time.h>
#include <netdb.h>

static SimpleMutex gClientRunMutex;
static i32 gClientRun;
static u16 gAuthServerPort;
static SimpleLanClientInfo gCurrentClientInfo;
static SimpleSocket gClientSocket;
static i32 gClientAlive;

typedef struct ClientAuthSettings
{
    u16 Port;
    u16 ServerPort;
    u32 Address;
    u32 ServerAddress;

    SimpleLanClientInfo Info;
} ClientAuthSettings;

void some_get_ip_code();

void*
_client_proccess_terminal_input(void* data)
{
    usleep(1000);
    printf("[Terminal Input]\n");
    while (1)
    {
	char typedCmd[20] = {};
	printf(">");
	scanf("%20s", typedCmd);

	if (string_compare(typedCmd, "exit"))
	{
	    GINFO("exit cmd typed!\n");
	    simple_thread_mutex_lock(&gClientRunMutex);
	    gClientRun = 0;
	    simple_thread_mutex_unlock(&gClientRunMutex);
	}
    }
}

void
client_main_proccess(ClientAuthSettings* pClientAuthSet)
{
    SimpleSocketSetting set = {
	.Address = pClientAuthSet->Address,
	.Port = pClientAuthSet->Port
    };
    gClientSocket = simple_socket_udp_create(set);

    printf("Connecting to server %s:%d..\n", ip_to_string(pClientAuthSet->ServerAddress), pClientAuthSet->ServerPort);
    simple_socket_connect(gClientSocket, pClientAuthSet->ServerAddress, pClientAuthSet->ServerPort);
    simple_socket_make_async(&gClientSocket);

    SimpleLanAuthRequest authReq = {
	.Base = {
	    .Type = SimpleLanRequestType_Auth
	},
	.ClientInfo = pClientAuthSet->Info
    };

    simple_socket_send(gClientSocket, &authReq, sizeof(authReq));
    printf("Authentication request sended!\n");

    fd_set readSet, writeSet;

    while (1)
    {
	simple_thread_mutex_lock(&gClientRunMutex);
	if (gClientRun == 0)
	    break;
	simple_thread_mutex_unlock(&gClientRunMutex);

	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);

	FD_SET(gClientSocket.Descriptor, &readSet);
	FD_SET(gClientSocket.Descriptor, &writeSet);

	// Select zone
	struct timeval timeout = {
	    .tv_sec = 15,
	    .tv_usec = 0
	};
	i32 maxNfds = gClientSocket.Descriptor + 1;
	i32 selectResult = select(maxNfds, &readSet, &writeSet, NULL, NULL);
	if (selectResult == -1)
	{
	    printf("Select error!\n");
	    perror("[select]");
	    vguard(0 && "Select error");
	}

	if (FD_ISSET(gClientSocket.Descriptor, &readSet))
	{
	    SimpleLanBaseResponse baseRes = {};
	    i32 recvBytes = simple_socket_recv_ext(gClientSocket, &baseRes, sizeof(SimpleLanBaseResponse), MSG_PEEK);
	    if (recvBytes <= 0)
		continue;

	    switch (baseRes.Type)
	    {

	    case SimpleLanResponseType_Auth_Registered:
		SimpleLanAuthResponse authRes = {};
		simple_socket_recv(gClientSocket, &authRes, sizeof(SimpleLanAuthResponse));

		gCurrentClientInfo = authRes.ClientInfo;

		printf("[User Registered on Server] Nick=%s Id=%d EndPoint=%s:%d\n", gCurrentClientInfo.Nickname, gCurrentClientInfo.Id, ip_to_string(gClientSocket.Address), gClientSocket.Port);
		break;

	    case SimpleLanResponseType_Error_NotRegisteredRequest:
		printf("[Not registered on server]\n");
		break;

	    default:
		GERROR("smth wrong but we go on!\n");
		break;

	    }

	}

	if (FD_ISSET(gClientSocket.Descriptor, &writeSet))
	{
	    static time_t ttime = 0;
	    if (ttime == 0)
		ttime = time(NULL);
	    i32 secondsDiff = time(NULL) - ttime;
	    if (secondsDiff > 15)
	    {
		SimpleLanAliveRequest aliveReq = {
		    .Base = {
			.Type = SimpleLanRequestType_Alive
		    },
		    .IsAlive = 1
		};

		simple_socket_send(gClientSocket, &aliveReq, sizeof(SimpleLanAliveRequest));

		ttime = time(NULL);
	    }

	}
    }

    SimpleLanBaseRequest logoutReq = {
	.Type = SimpleLanRequestType_Logout
    };
    simple_socket_send(gClientSocket, &logoutReq, sizeof(SimpleLanBaseRequest));
}

u32
_client_ip_parse(char* str)
{
    u32 addr = 0;

    char** strs = string_split(str);

    if (array_count(strs) != 4)
    {
	GERROR("Wrong ip address %s!\n", str);
	vguard(0);
    }

    u8 b0 = (u8) string_to_i32(strs[0]);
    u8 b1 = (u8) string_to_i32(strs[1]);
    u8 b2 = (u8) string_to_i32(strs[2]);
    u8 b3 = (u8) string_to_i32(strs[3]);

    return ip_as_integer(b0, b1, b2, b3);
}

#if defined(DEFINE_MAIN)
i32
main(i32 argc, char** argv)
#else
    i32
    client_run(i32 argc, char** argv)
#endif
{
    char* prog    = argv[0];
    char* nick    = argv[1];
    char* ipva    = argv[2];
    char* sepo    = argv[3];
    char* loIp = argv[4];

    u32 serverIpAddress = _client_ip_parse(ipva);
    u16 serverPort = string_to_i32(sepo);
    u32 localIpAddress = _client_ip_parse(loip);
    GINFO("Program: %s Nickname: %s ServIp: %s ServerPort: %d LocalIp: %s\n", prog, nick, ip_to_string(serverIpAddress), sepo, localIpAddress);

    gClientRunMutex = simple_thread_mutex_create();
    gClientRun = 1;
    gAuthServerPort = 13370;

    SimpleTapSettings tapSet = {
	.PreferredTapName = "tap0",
	.IpAddress = localIpAddress, //ip_as_integer(10, 0, 0, 7),
	.MaskBits = 24
    };
    SimpleTap simpleTap = tap_create(tapSet);
    tap_connect(&simpleTap, 1);
    GINFO("Start tap on ip: %s\n", ip_to_string(simpleTap.VlanIp4));

    SimpleThread simpleThread = simple_thread_create(_client_proccess_terminal_input, 1024, NULL);

    SimpleLanClientInfoSettings set = {
	.Nickname = argv[1]
    };
    SimpleLanClientInfo clientInfo = simple_lan_client_info_create(set);

    ClientAuthSettings clientAuthSet = {
	.Address = INADDR_LOOPBACK,
	.Port = serverPort,
	.ServerAddress = serverIpAddress, //ip_as_integer(127, 0, 0, 1),
	.ServerPort = gAuthServerPort,
	.Info = clientInfo
    };
    client_main_proccess(&clientAuthSet);

    printf("Close client connection ...\n");
    return 0;
}















#if 0
#include <stdio.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
void
some_get_ip_code()
{

#if 0

    char hostName[256] = {};
    if (gethostname(hostName, 256) != 0)
    {
	GERROR("Can't get host name!\n");
    }
    GINFO("Hostname: %s\n", hostName);

    struct addrinfo* meAddrInfo = NULL;
    struct addrinfo hints = {
	.ai_family = AF_INET,
	.ai_socktype = SOCK_DGRAM
    };
    if (getaddrinfo(hostName, NULL,
		    &hints, &meAddrInfo) != 0)
    {
	GERROR("Can't get address info!\n");
    }
    u32 mineIp = ((struct sockaddr_in*)meAddrInfo->ai_addr)->sin_addr.s_addr;
    GINFO("IP: %s\n", ip_to_string(ntohl(mineIp)));
    freeaddrinfo(meAddrInfo);

#else
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next)
    {
	if (!ifa->ifa_addr)
	    continue;

	if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
	    // is a valid IP4 Address
	    tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
	    char addressBuffer[INET_ADDRSTRLEN];
	    inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
	    printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
	}
	else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
	    // is a valid IP6 Address
	    tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
	    char addressBuffer[INET6_ADDRSTRLEN];
	    inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
	    printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
	}
    }

    if (ifAddrStruct!=NULL)
	freeifaddrs(ifAddrStruct);
#endif

}
#endif
