#ifndef SIMPLE_SOCKET_H
#define SIMPLE_SOCKET_H

#include <netinet/in.h> // sockaddr_in
#include <Utils/Types.h>

/*
  DOCS(typedef): Все данные по сети передаются как big endian,
  так что их нужно htonl() чтобы конвертнуть назад в little endian.
  Данные функции, как и все api-функции работают в стандартной архитектуре,
  то есть little endian
*/
u32 ip_as_integer(u8 ip3, u8 ip2, u8 ip1, u8 ip0);
void ip_as_string(char str[], u32 ip);
void ip_print(u32 address);
char* ip_to_string(u32 address);
u32 htonf(f32 f);
f32 ntohf(u32 p);

typedef struct SimpleSocket
{
    i8 IsAsync;
    u16 Port;
    u32 Address;
    i32 Descriptor;
} SimpleSocket;

typedef struct SimpleSocketSetting
{
    i8 IsServer;
    i8 IsAsync;
    u16 Port;
    u32 Address;
    i32 MaxClients;
} SimpleSocketSetting;

SimpleSocket simple_socket_tcp_create(SimpleSocketSetting set);
SimpleSocket simple_socket_udp_create(SimpleSocketSetting set);
i32 simple_socket_connect(SimpleSocket socket, u32 address, u16 port);
/*
  DOCS(typedef): TCP
*/
i32 simple_socket_send_ext(SimpleSocket socket, void* data, size_t size, i32 flags);
i32 simple_socket_send(SimpleSocket socket, void* data, size_t size);
i32 simple_socket_recv_ext(SimpleSocket socket, void* data, size_t size, i32 flags);
i32 simple_socket_recv(SimpleSocket socket, void* data, size_t size);
/*
  DOCS(typedef): UDP
*/
i32 simple_socket_send_to_ext(SimpleSocket from, struct sockaddr_in* to , void* data, i32 size, u32 flags);
i32 simple_socket_send_to(SimpleSocket from, SimpleSocket to, void* data, size_t size);
i32 simple_socket_recv_from_ext(SimpleSocket to, struct sockaddr_in* from, void* data, i32 size, u32 flags);
i32 simple_socket_recv_from(SimpleSocket to, SimpleSocket from, void* data, i32 size);

SimpleSocket simple_socket_accept(SimpleSocket socket);
void simple_socket_bind(SimpleSocket socket, u32 address, u16 port);
void simple_socket_make_async(SimpleSocket* pSocket);
i32 simple_socket_close(SimpleSocket socket);
void simple_socket_print(SimpleSocket socket);

typedef enum ConnectionProtocolType
{
    ConnectionProtocolType_TCP = 0,
    ConnectionProtocolType_UDP
} ConnectionProtocolType;

typedef enum IpType
{
    IpType_V4 = 0,
    IpType_V6
} IpType;

void sockets_test();

#endif // SIMPLE_SOCKET_H
