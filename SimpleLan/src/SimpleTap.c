#include "SimpleTap.h"

#include <Utils/SimpleThread.h>
#include "SimpleSocket.h"
#include <Utils/SimpleStandardLibrary.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>

//#include <sys/time.h>
//#include <sys/socket.h>
//#include <net/if.h>
//#include <net/route.h>

typedef struct ifreq_FluentApi
{
    struct ifreq Request;
    struct ifreq_FluentApi* (*set_name) (char* name);
    struct ifreq_FluentApi* (*set_addr4) (u64 addrAsVal);
    struct ifreq_FluentApi* (*set_flag) (u16 flag);
} ifreq_FluentApi;

static ifreq_FluentApi g_IfrequestFluentApi;

// DOCS(typedef): Fluent api

ifreq_FluentApi*
set_name(char* name)
{
    struct ifreq* req = (struct ifreq*) &g_IfrequestFluentApi.Request;
    strncpy(req->ifr_name, name, IFNAMSIZ);
    req->ifr_name[IFNAMSIZ - 1] = 0x00;
    return &g_IfrequestFluentApi;
}

ifreq_FluentApi*
set_addr4(u64 addrAsVal)
{
    struct ifreq* req = (struct ifreq*) &g_IfrequestFluentApi.Request;
    struct sockaddr_in* pAddr = (struct sockaddr_in*) &req->ifr_addr;
    pAddr->sin_family = AF_INET;
    //htonl((0xFFFFFFFF >> (32 - maskInBits)) << (32 - maskInBits));
    pAddr->sin_addr.s_addr = addrAsVal;
    return &g_IfrequestFluentApi;
}

/*
  DOCS(typedef): IIF_TUN, IFF_TAP, IFF_NO_PI
*/
ifreq_FluentApi*
set_flag(u16 flag)
{
    struct ifreq* req = (struct ifreq*) &g_IfrequestFluentApi.Request;
    /*
      DOCS(typedef): Flags
      IFF_TUN   - TUN device (no Ethernet headers)
      IFF_TAP   - TAP device
      IFF_NO_PI - Do not provide packet information
    */
    req->ifr_flags = flag;//IFF_TAP;
    return &g_IfrequestFluentApi;
}

ifreq_FluentApi*
ifr_helper_create()
{
    g_IfrequestFluentApi = (ifreq_FluentApi) {
	.set_name = set_name,
	.set_addr4 = set_addr4,
	.set_flag = set_flag
    };
    return &g_IfrequestFluentApi;
}

i32
sys(i32 fileDescriptor, i32 reqFlag, struct ifreq* req, const char* errorLog)
{
    i32 err = ioctl(fileDescriptor, reqFlag, (void *) req);
    if (err < 0)
    {
	GERROR("[ERROR] %s\n", errorLog);
	perror("ioctl");
	return 0;
    }

    return 1;
}

i32
_get_socket()
{
    const i32 socketFd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketFd < 0)
    {
	printf("Socket creation error!\n");
	vguard(0);
	return 0;
    }

    return socketFd;
}

/*
  DOCS(typedef): Needs to be called with sudo (root)
  ping -I tap0 10.1.7.14
  ping -I tap0 10.0.0.7
*/
SimpleTap
tap_create(SimpleTapSettings set)
{
    printf("Hello!\n");
    GINFO("SIZEOF: %d\n", sizeof(long unsigned int));

    printf("uid: %d, isroot: %d\n", getuid(), getuid() == 0);

    SimpleTap simpleTap = {};

    char tapName[64] = "/dev/net/tap";
    if (access(tapName, F_OK) == -1)
    {
	// No tap found, create one.
	/*
	  mode_t:
	  S_IFCHR Character-special (non-portable)
	  S_IRUSR Read by owner.
	  S_IWUSR Write by owner
	*/
	mode_t flags = S_IFCHR | S_IRUSR | S_IWUSR;
	dev_t dev = makedev(10, 200);
	if (mknod(tapName, flags, dev) == -1)
	{
	    GERROR("Fatal!\n\n\n");
	    vguard(0);
	}

	GSUCCESS("Create tap in /dev/net/tap!\n");
    }

    i32 tapFd = open(tapName, O_RDWR);
    if (!tapFd)
    {
	GERROR("Tan can't be open\n");
	vguard(0);
    }

    // DOCS(typedef): Request tap device name
    printf("[SUCCESS] Tan open %s\n", tapName);
    char* devName = set.PreferredTapName;
    ifreq_FluentApi* pReqDevName =
	ifr_helper_create()->set_name(devName)
	/*
	  DOCS(typedef): Flags
	  IFF_TUN   - TUN device (no Ethernet headers)
	  IFF_TAP   - TAP device
	  IFF_NO_PI - Do not provide packet information
	*/
	->set_flag((IFF_TAP | IFF_NO_PI));
    if (sys(tapFd, TUNSETIFF, &pReqDevName->Request, "Device error") == 0)
    {
	GERROR("Can't set dev name!\n");
	vguard(0);
    }
    printf("tap device name: %s\n", devName);

    // DOCS(typedef): Get MAC address
    ifreq_FluentApi* pReqMacAddr =
	ifr_helper_create()->set_name(devName);
    if (sys(_get_socket(), SIOCGIFHWADDR, &pReqMacAddr->Request, "Get MAC-address") == 0)
    {
	GERROR("Get mac address!\n");
	vguard(0);
    }
    u8 mac[6] = {
	[0] = (u8) pReqMacAddr->Request.ifr_hwaddr.sa_data[0],
	[1] = (u8) pReqMacAddr->Request.ifr_hwaddr.sa_data[1],
	[2] = (u8) pReqMacAddr->Request.ifr_hwaddr.sa_data[2],
	[3] = (u8) pReqMacAddr->Request.ifr_hwaddr.sa_data[3],
	[4] = (u8) pReqMacAddr->Request.ifr_hwaddr.sa_data[4],
	[5] = (u8) pReqMacAddr->Request.ifr_hwaddr.sa_data[5]
    };
    /* display result */
    printf("%.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    for (i32 i = 0; i < 6; ++i)
	simpleTap.MacAsBytes[i] = mac[i];

    // DOCS(typedef): Request MTU
    ifreq_FluentApi* pReqMtu =
	ifr_helper_create()->set_name(devName);
    if (sys(_get_socket(), SIOCGIFMTU, &pReqMtu->Request, "Get MAC-address") == 1)
    {
	i32 mtu = pReqMtu->Request.ifr_mtu;
	printf("Mtu: %d\n", mtu);

	// IP + UDP + FSCP HEADER + FSCP DATA HEADER
	const i32 static_payload_size = 20 + 8 + 4 + 22;
	i32 mss = mtu - static_payload_size;
	printf("Mss: %d\n", mss);

	simpleTap.Mtu = mtu;
	simpleTap.Mss = mss;
    }

    //DOCS(): Change ip to virtual one
    u64 addr = (u64) set.IpAddress; //(u64) ip_as_integer(10, 1, 7, 14);
    simpleTap.VlanIp4 = (u32) addr;
    ifreq_FluentApi* pReqIpChange =
	ifr_helper_create()->set_name(devName)->set_addr4(ntohl(addr));
    if (sys(_get_socket(), SIOCSIFADDR, &pReqIpChange->Request, "Set IP-address") == 0)
    {
	GERROR("Can't change ip address!\n");
    }
    sys(_get_socket(), SIOCGIFADDR, &pReqIpChange->Request, "Get ip add!");
    addr = ((struct sockaddr_in*) &pReqIpChange->Request.ifr_addr)->sin_addr.s_addr;
    GINFO("addr: %d %s\n", ip_to_string(addr));

    // DOCS(): Setting mask
    i32 maskInBits = set.MaskBits; //24;
    u32 mask = htonl((0xFFFFFFFF >> (32 - maskInBits)) << (32 - maskInBits));
    ifreq_FluentApi* pReqSetMask =
	ifr_helper_create()->set_name(devName)->set_addr4(mask);
    if (sys(_get_socket(), SIOCSIFNETMASK, &pReqSetMask->Request, "Set mask IP") == 0)
    {
	GERROR("Can't change ip address!\n");
    }

    fcntl(tapFd, F_SETFL, O_NONBLOCK);

    simpleTap.DeviceName = string(devName);
    simpleTap.Fd = tapFd;

    //close(tapFd);
    return simpleTap;
}

//NOTE() mb tap_up
void
tap_connect(SimpleTap* pSimpleTap, i32 connect)
{
    vguard(getuid() == 0 && "Not root user!");

    //SIOCGIFFLAGS
    ifreq_FluentApi* pReqUpInterface =
	ifr_helper_create()
	->set_name(pSimpleTap->DeviceName);
    if (sys(_get_socket(), SIOCGIFFLAGS, &pReqUpInterface->Request, "Get status of interface") == 0)
    {
	GERROR("Can't Get status of interface!\n");
	vguard(0);
    }

    i16 flags = connect ? (IFF_UP | IFF_RUNNING) : (~(IFF_UP | IFF_RUNNING));
    pReqUpInterface =
	pReqUpInterface
	->set_name(pSimpleTap->DeviceName)
	->set_flag(flags);
    if (sys(_get_socket(), SIOCSIFFLAGS, &pReqUpInterface->Request, "Up interface") == 0)
    {
	GERROR("Can't up interface!\n");
	vguard(0);
    }

}

void
tap_destroy(const char* name)
{
}
