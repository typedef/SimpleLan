#include "SimpleLan.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 0,
	.IsFrameRatePrinted = 0,
	.Name = "Demo",
    };
    application_create(appSettings);

    Layer layer = {
	.Name = "Demo Layer",
	.OnAttach = demo_on_attach,
	.OnUpdate = demo_on_update,
	.OnUIRender = demo_on_ui_render,
	.OnEvent = demo_on_event,
	.OnDestoy = demo_on_destroy,
    };

    application_push_layer(layer);
}
