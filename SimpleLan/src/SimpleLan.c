#include "SimpleLan.h"

#include <Utils/SimpleStandardLibrary.h>

SimpleLanClientInfo
simple_lan_client_info_create_raw(char* nickName, i32 id)
{
    SimpleLanClientInfo info = {

    };

    return info;
}

SimpleLanClientInfo
simple_lan_client_info_create(SimpleLanClientInfoSettings set)
{
    i64 nicknameLen = string_length(set.Nickname);
    if (nicknameLen > 32)
    {
	GERROR("Nickname should be less 32\n");
	vguard(0);
    }

    SimpleLanClientInfo clientInfo = {
    };

    memset(clientInfo.Nickname, 0, 32);
    memcpy(clientInfo.Nickname, set.Nickname, string_length(set.Nickname));

    return clientInfo;
}

void
simple_lan_client_info_print(SimpleLanClientInfo clientInfo)
{
    printf("[Client Info] nick=%s id=%d\n", clientInfo.Nickname, clientInfo.Id);
}

const char*
simple_lan_request_type_to_string(SimpleLanRequestType type)
{
    switch (type)
    {
    case SimpleLanRequestType_None: return "None";
    case SimpleLanRequestType_Auth: return "Auth";
    case SimpleLanRequestType_Alive: return "Alive";
    case SimpleLanRequestType_Logout: return "Logout";
    case SimpleLanRequestType_Resend: return "Resend";
    case SimpleLanRequestType_Error: return "Error";
    }

    vguard(0);
    return NULL;
}

const char*
simple_lan_response_type_to_string(SimpleLanResponseType type)
{
    switch (type)
    {

    case SimpleLanResponseType_None: return "None";
    case SimpleLanResponseType_Auth_Registered: return "Auth Registered";
    case SimpleLanResponseType_Error_NotRegisteredRequest: return "Not Registered Request";

    }

    vguard(0 && "Wrong response type!");
    return NULL;
}

void
simple_lan_auth_request_print(SimpleLanAuthRequest slar)
{
    printf("[AuthReq] %d %s\n", slar.ClientInfo.Id, slar.ClientInfo.Nickname);
}
