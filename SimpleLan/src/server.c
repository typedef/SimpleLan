#include <Utils/SimpleThread.h>
#include <Utils/SimpleStandardLibrary.h>

#include "SimpleSocket.h"
#include "SimpleLan.h"
#include <math.h>

static SimpleMutex gServerRunMutex;
static i32 gServerRun;
static u32 gAuthServerIp;
static u32 gClientsServerIp;
static u16 gClientsPort;
static i32 gMaxClients;
static i32 gLastClientId;
static i32 gSecondsToBeAlive;

/*
  double difftime (time_t time1, time_t time0)
*/

typedef struct ServerClient
{
    struct sockaddr_in Address;
    u16 Port;
    SimpleLanClientInfo Info;
    time_t TimeAlive;
} ServerClient;
static ServerClient* gRegisteredClients = NULL;
static ServerClient* gClients = NULL;

struct ServerCreateSettings
{
    u16 Port;
    u32 Address;
};

u32
_server_get_ip_helper(struct sockaddr_in address)
{
    return (u32) ntohl(address.sin_addr.s_addr);
}
u16
_server_get_port_helper(struct sockaddr_in address)
{
    return (u16) ntohs(address.sin_port);
}

static i32
_client_get_index(u32 address, u16 port)
{
    i32 i,
	count = array_count(gRegisteredClients);

    for (i = 0; i < count; ++i)
    {
	ServerClient serverClient = gRegisteredClients[i];
	struct sockaddr_in siaddress = serverClient.Address;
	u32 caddress = (u32) ntohl(siaddress.sin_addr.s_addr);
	u16 cport = (u16) ntohs(siaddress.sin_port);

	if (caddress == address && cport == port)
	    return i;
    }

    return -1;
}

void
_server_print(char* tagMessage, ServerClient serverClient)
{
    GINFO("[%s] %s[%d] EndPoint: %s:%d\n",
	  tagMessage,
	  serverClient.Info.Nickname,
	  serverClient.Info.Id,
	  ip_to_string(_server_get_ip_helper(serverClient.Address)),
	  _server_get_port_helper(serverClient.Address));
}

i32
_server_remove_client(ServerClient serverClient, i32* forIndex)
{
    array_remove(gRegisteredClients, serverClient);

    if (forIndex == NULL)
	return 0;

    i32 indexVal = *forIndex;
    if ((indexVal + 1) >= array_count(gRegisteredClients))
	return 1;

    *forIndex = indexVal - 1;

    return 0;
}

i32
_server_unregister_client(ServerClient serverClient, i32* forIndex)
{
    --gLastClientId;
    return _server_remove_client(serverClient, forIndex);
}

void
_server_request_logout(SimpleSocket serverSocket, ServerClient serverClient)
{
    _server_print("User Logout", serverClient);
    SimpleLanAuthRequest authReq = {};
    i32 returnResult = simple_socket_recv_from_ext(serverSocket, &serverClient.Address, &authReq, sizeof(SimpleLanAuthRequest), 0);
    _server_unregister_client(serverClient, NULL);
}

void
_server_process_unauthorized_user(SimpleSocket serverSocket, SimpleLanBaseRequest baseRequest, ServerClient* pServerClient)
{
    switch (baseRequest.Type)
    {

    case SimpleLanRequestType_Auth:
    {
	SimpleLanAuthRequest authReq = {};
	struct sockaddr_in clientAddress;
	i32 returnResult = simple_socket_recv_from_ext(serverSocket, &clientAddress, &authReq, sizeof(SimpleLanAuthRequest), 0);

	ServerClient serverClient = {
	    .Address = clientAddress,
	    .Info = authReq.ClientInfo,
	    .TimeAlive = time(NULL),
	};
	serverClient.Info.Id = gLastClientId;

	SimpleLanAuthResponse authRes = {
	    .Base = {
		.Type = SimpleLanResponseType_Auth_Registered
	    },
	    .ClientInfo = serverClient.Info
	};

	simple_socket_send_to_ext(serverSocket, &serverClient.Address, &authRes, sizeof(SimpleLanAuthResponse), 0);

	array_push(gRegisteredClients, serverClient);
	++gLastClientId;

	*pServerClient = serverClient;
	_server_print("User Registered", serverClient);
	break;
    }

    default:
    {
	SimpleLanAuthRequest authReq = {};
	struct sockaddr_in clientAddress;
	i32 returnResult = simple_socket_recv_from_ext(serverSocket, &clientAddress, &authReq, sizeof(SimpleLanAuthRequest), 0);

	// DOCS(typedef): send Error to Client
	const char* toStr = simple_lan_request_type_to_string(baseRequest.Type);
	GINFO("[Unauthorized request] ReqType: %s\n", toStr);

	SimpleLanErrorResponse errorResponse = {
	    .Base = {
		.Type = SimpleLanResponseType_Error_NotRegisteredRequest,
		.Size = 59
	    },
	    .Message = "You should authorize first before sending any other request"
	};
	simple_socket_send_to_ext(serverSocket,
				  &clientAddress,
				  &errorResponse,
				  sizeof(SimpleLanErrorResponse) + errorResponse.Base.Size,
				  0);
	break;
    }

    }

}

void
_server_process_authorized_user(SimpleSocket serverSocket, ServerClient serverClient)
{
#if 0
    SimpleLanBaseRequest baseRequest = {};
    i32 bytesRecv = simple_socket_recv_ext(serverClient.Socket, &baseRequest, sizeof(SimpleLanBaseRequest), MSG_PEEK);
    if (bytesRecv <= 0)
    {
	GWARNING("No data!\n");
	vguard(0);
	/* SimpleLanAliveRequest aliveReq = {}; */
	/* simple_socket_recv_ext(serverClient.Socket, &aliveReq, sizeof(SimpleLanAliveRequest), MSG_PEEK); */
	/* GERROR("Alive: %s %d\n", */
	/*       simple_lan_request_type_to_string(aliveReq.Base.Type), */
	/*       aliveReq.IsAlive); */
	return;
    }

    if (baseRequest.Type == SimpleLanRequestType_Alive)
    {
	SimpleLanAliveRequest aliveRequest = {};
	bytesRecv = simple_socket_recv(serverClient.Socket, &aliveRequest, sizeof(SimpleLanAliveRequest));
	_server_print("User still connected", serverClient);
    }
    else if (baseRequest.Type == SimpleLanRequestType_Logout)
    {
	_server_request_logout(serverSocket, serverClient);
    }
#endif
}

void
server_run(SimpleSocketSetting* pSet)
{
    SimpleSocket serverSocket = simple_socket_udp_create(*pSet);
    simple_socket_make_async(&serverSocket);

    fd_set readSet, writeSet, errorSet;

    while (1)
    {
	simple_thread_mutex_lock(&gServerRunMutex);
	if (!gServerRun)
	{
	    break;
	}
	simple_thread_mutex_unlock(&gServerRunMutex);

	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);
	FD_ZERO(&errorSet);

	FD_SET(serverSocket.Descriptor, &readSet);
	FD_SET(serverSocket.Descriptor, &writeSet);
	FD_SET(serverSocket.Descriptor, &errorSet);

	// DOCS(typedef): Add every client socket to fd_set
	i32 registeredCount = array_count(gRegisteredClients);
	//GINFO("cnt: %d\n", registeredCount);
	for (i32 i = 0; i < registeredCount; ++i)
	{
	    ServerClient serverClient = gRegisteredClients[i];

	    // DOCS(typedef): Alive update
	    time_t currentTime = time(NULL);
	    f64 diffSeconds = fabs(difftime(serverClient.TimeAlive, currentTime));
	    if (diffSeconds >= gSecondsToBeAlive)
	    {
		// DOCS(typedef): Deleting 'dead' clients
		i32 isSkip = _server_unregister_client(serverClient, &i);

		_server_print("Dead client", serverClient);

		if (isSkip)
		    continue;
	    }
	}

	// Select zone
	struct timeval timeout = {
	    .tv_sec = 15,
	    .tv_usec = 0
	};
	i32 maxNfds = serverSocket.Descriptor + 1;
	i32 selectResult = select(maxNfds, &readSet, &writeSet, &errorSet, NULL);
	if (selectResult == -1)
	{
	    printf("Select error!\n");
	    perror("[select]");
	    vguard(0 && "Select error");
	}

	if (FD_ISSET(serverSocket.Descriptor, &readSet))
	{
	    /*
	      1. check if user registered or not
	      2_if_t. get user info, process_request
	      2_if_f. check if registration requested
	      2_if_e any request wo reg = error response
	    */


	    SimpleLanBaseRequest baseReq = {};
	    struct sockaddr_in recvAddress;
	    i32 isDataRecv = simple_socket_recv_from_ext(
		serverSocket,
		&recvAddress,
		&baseReq,
		sizeof(SimpleLanBaseRequest),
		MSG_PEEK);
	    if (!isDataRecv)
		continue;

	    // get user by address
	    u32 address = _server_get_ip_helper(recvAddress);
	    u16 port = _server_get_port_helper(recvAddress);
	    i32 clientIndex = _client_get_index(address, port);

	    if (clientIndex == -1)
	    {
		// Unauthorized
		ServerClient serverClient;
		_server_process_unauthorized_user(serverSocket, baseReq, &serverClient);
	    }
	    else
	    {
		// Authorized
		GINFO("Authorized!\n");

		simple_socket_recv_from_ext(
		    serverSocket,
		    &recvAddress,
		    &baseReq,
		    sizeof(SimpleLanBaseRequest),
		    0);

		switch (baseReq.Type)
		{

		case SimpleLanRequestType_Alive:
		    _server_print("Alive", gRegisteredClients[clientIndex]);
		    gRegisteredClients[clientIndex].TimeAlive = time(NULL);
		    break;

		}

	    }

	    /* GINFO("Base req: %s %s\n", */
	    /*	  simple_lan_request_type_to_string(baseReq.Type), */
	    /*	  ip_to_string(htonl(recvAddress.sin_addr.s_addr)) */
	    /*	); */

	}



#if 0
	for (i32 i = 0; i < array_count(gRegisteredClients); ++i)
	{
	    ServerClient serverClient = gRegisteredClients[i];

	    if (FD_ISSET(serverClient.Socket.Descriptor, &readSet))
	    {
		if (serverClient.IsAuthorized)
		{
		    _server_process_authorized_user(serverSocket, serverClient);
		}
		else
		{
		    _server_process_unauthorized_user(serverSocket, serverClient, i);
		}
	    }

	    if (FD_ISSET(serverClient.Socket.Descriptor, &errorSet))
	    {
		printf("Error set!\n");
		perror("[weird error set]");
	    }
	}
#endif
    }

    // log
    simple_socket_close(serverSocket);
}

void
server_create_and_run()
{
    gServerRunMutex = simple_thread_mutex_create();

    gServerRun = 1;
    gClientsServerIp = INADDR_ANY; ip_print(gClientsServerIp);
    gClientsPort = 13370;
    gMaxClients = 100;
    gLastClientId = 1;
    gSecondsToBeAlive = 30;

    SimpleSocketSetting authSet = {
	.IsServer = 1,
	.Address = gClientsServerIp,
	.Port = gClientsPort,
	.MaxClients = gMaxClients
    };

    char ipAsStr[32];
    ip_as_string(ipAsStr, authSet.Address);
    GINFO("Start Authentication Thread ip=%s port=%d!\n", ipAsStr, authSet.Port);
    server_run(&authSet);

    GINFO("Close server ...\n");
}
