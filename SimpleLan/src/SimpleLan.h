#ifndef SIMPLE_LAN_H
#define SIMPLE_LAN_H

#include <Utils/Types.h>

typedef struct SimpleLanClientInfoSettings
{
    char* Nickname;
} SimpleLanClientInfoSettings;

typedef struct SimpleLanClientInfo
{
    i32 Id;
    char Nickname[32];
} SimpleLanClientInfo;

SimpleLanClientInfo simple_lan_client_info_create(SimpleLanClientInfoSettings set);
void simple_lan_client_info_print(SimpleLanClientInfo clientInfo);

typedef enum SimpleLanRequestType
{
    SimpleLanRequestType_None = 0,
    SimpleLanRequestType_Auth,
    SimpleLanRequestType_Alive,
    SimpleLanRequestType_Logout,
    SimpleLanRequestType_Resend,
    SimpleLanRequestType_Error,
} SimpleLanRequestType;
const char* simple_lan_request_type_to_string(SimpleLanRequestType type);

typedef enum SimpleLanResponseType
{
    SimpleLanResponseType_None = 0,
    SimpleLanResponseType_Auth_Registered,
    SimpleLanResponseType_Error_NotRegisteredRequest,
} SimpleLanResponseType;
const char* simple_lan_response_type_to_string(SimpleLanResponseType type);

typedef struct SimpleLanBaseRequest
{
    SimpleLanRequestType Type;
    size_t Size;
} SimpleLanBaseRequest;

/*
  DOCS(typedef): Authentication Block
*/
typedef struct SimpleLanAuthRequest
{
    SimpleLanBaseRequest Base;
    SimpleLanClientInfo ClientInfo;
} SimpleLanAuthRequest;

typedef struct SimpleLanBaseResponse
{
    SimpleLanResponseType Type;
    size_t Size;
} SimpleLanBaseResponse;

typedef struct SimpleLanAuthResponse
{
    SimpleLanBaseResponse Base;
    SimpleLanClientInfo ClientInfo;
} SimpleLanAuthResponse;

/*
  DOCS(typedef): Authentication Block
*/
typedef struct SimpleLanAliveRequest
{
    SimpleLanBaseRequest Base;
    i32 IsAlive;
} SimpleLanAliveRequest;

typedef struct SimpleLanResendRequest
{
    SimpleLanBaseRequest Base;
    size_t Size;
    void* Data;
} SimpleLanResendRequest;

typedef struct SimpleLanErrorResponse
{
    SimpleLanBaseResponse Base;
    void* Message;
} SimpleLanErrorResponse;

void simple_lan_auth_request_print(SimpleLanAuthRequest slar);

#endif // SIMPLE_LAN_H
