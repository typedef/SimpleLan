#ifndef KEY_BINDING_H
#define KEY_BINDING_H

#include <Utils/Types.h>

typedef struct BindingElement
{
    i32 KeyCode;
    i32 Action;
    i32 Modificator;
} BindingElement;

i8 binding_element_equals(BindingElement a, BindingElement b);
void binding_element_format(char buf[64], BindingElement a);
void binding_element_print(BindingElement a);

typedef void (*KeyBindingCallbackDelegate)();

typedef struct MinorBinding
{
    WideString Description;
    KeyBindingCallbackDelegate Function;
    BindingElement* Binds;
} MinorBinding;

MinorBinding minor_binding_create(WideString description, KeyBindingCallbackDelegate function, BindingElement* binds);

typedef struct KeyBindingCreationRecord
{
    BindingElement Major;
    MinorBinding Minor;
} KeyBindingCreationRecord;

typedef struct KeyBinding
{
    BindingElement Major;
    MinorBinding* Minors;
} KeyBinding;


i8 key_binding_equals(KeyBinding aKeyBinding, KeyBinding bKeyBinding);
i8 key_binding_is_registered(KeyBindingCreationRecord newBind);
void key_binding_register(KeyBindingCreationRecord keyBinding);
void key_binding_add(BindingElement bindingElement);
BindingElement* key_binding_get();
void key_binding_process();


#endif // KEY_BINDING_H
