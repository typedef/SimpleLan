#ifndef SIMPLE_INPUT_H
#define SIMPLE_INPUT_H

#include <Utils/Types.h>
#include <InputSystem/KeyCodes.h>

void input_init(void* window);
i8 input_is_key_pressed(KeyType type);
i8 input_is_key_release(KeyType type);
i8 input_is_mouse_pressed(MouseButtonType type);
void input_get_cursor_position(double* xpos, double* ypos);
void input_set_cursor_position(double xpos, double ypos);
v2 input_get_cursor_position_v2();
void input_enable_cursor();
void input_disable_cursor();
void input_set_cursor_mode(CursorMode mode);
i8 input_is_cursor_disabled();

#endif // SIMPLE_INPUT_H
