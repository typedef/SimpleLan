#ifndef SUIF_BACKEND_H
#define SUIF_BACKEND_H

typedef struct Renderer2d Renderer2d;
typedef struct Event Event;

void sui_backend_new_frame();
void sui_backend_draw(Renderer2d* renderer);
void sui_backend_event(Event* event);
void sui_backend_window_resized();

#endif // SUIF_BACKEND_H
