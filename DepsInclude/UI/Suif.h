#ifndef SUIF_H
#define SUIF_H

#include <Utils/Types.h>
#include <Utils/Font.h>
#include <Event/Event.h>

/*
	 ####################################################
	     DOCS(typedef): Base Sui Types && Functions
	 ####################################################
*/
typedef i64 SuiPanelId;
typedef i64 SuiWidgetId;
#define GetSuiPanelId(label) ((SuiPanelId) ((u64)label))
#define GetSuiWidgetId(label) ((SuiWidgetId) ((u64)label))

typedef enum SuiState
{
    SuiState_None    = 0,
    SuiState_Hovered = 1 << 0,
    SuiState_Clicked = 1 << 1,
    SuiState_Grabbed = 1 << 2
} SuiState;

const char* sui_state_to_string(SuiState suiState);
void sui_state_print(SuiState suiState);
f32 sui_apply_offset(f32 fullSize, f32 marginValue, f32 value);
v3 sui_apply_offset_v3(v3 value, v2 fullSize, v2 offset);
f32 sui_get_text_width(WideString wstr);
void sui_get_text_metrics_ext(wchar* buffer, i32 length, f32* width, f32* height);
f32 sui_get_char_xoffset(wchar wc);
void sui_get_text_metrics(WideString wstr, f32* width, f32* height);

/*
	 #####################################
	       DOCS(typedef): Sui Input
	 #####################################
*/
typedef enum SuiKeys
{
    SuiKeys_Start = 0,

    SuiKeys_Mouse_Left_Button,
    SuiKeys_Mouse_Right_Button,
    SuiKeys_Mouse_Middle_Button,

    SuiKeys_Tab,
    SuiKeys_Backspace,

    SuiKeys_LeftArrow,
    SuiKeys_RightArrow,
    SuiKeys_UpArrow,
    SuiKeys_DownArrow,
    SuiKeys_Home,
    SuiKeys_End,

    SuiKeys_Delete,

    SuiKeys_Count
} SuiKeys;

typedef enum SuiKeyState
{
    SuiKeyState_None = 0,
    SuiKeyState_Pressed,
    SuiKeyState_Released,
} SuiKeyState;

typedef struct SuiKeyData
{
    SuiKeyState State;
    i8 IsHandled;
    /* NOTE(typedef): Maybe later try analog values here */
} SuiKeyData;

typedef enum SuiModeType
{
    SuiModeType_Insert = 0,
    SuiModeType_Overwrite
} SuiModeType;

typedef struct SuiInputData
{
    v2 MousePosition;
    v2 PrevMousePosition;
    v2 GrabPosition;
    v2 GrabMousePosition;
    i8 MousePositionChanged;
    i8 LeftKeyWasPressed;
    SuiKeyData KeyData[SuiKeys_Count];
    i32 CharTyped;

    i8 IsInputAlreadyHandled;

    /* SuiWidgetId LastFocusedID; */
    SuiWidgetId FocusID;

    i32 TextIndexPosition;
    SuiModeType Mode;

    /* /\* Input *\/ */

} SuiInputData;

/*
	 #####################################
	       DOCS(typedef): Sui Widgets
	 #####################################
*/
typedef enum SuiWidgetType
{
    SuiWidgetType_Button = 0,
    SuiWidgetType_Slider_F32,
    SuiWidgetType_Checkbox,
    SuiWidgetType_Text,
    SuiWidgetType_InputString,
    SuiWidgetType_Dropdown,
    SuiWidgetType_Image,


    SuiWidgetType_SelectList,
    SuiWidgetType_ColorPicker,
    SuiWidgetType_Count
} SuiWidgetType;


typedef struct SuiWidget
{
    SuiWidgetId Id;
    SuiWidgetType Type;
    WideString* Label;
    SuiState State;
    SuiState SubState;
    v2 Size;
    v3 Position;

    //TODO(typedef): DELETE THIS
    i32 StillExist;

    WideString TempBuffer;

    union
    {
	v4 FloatValues;

	struct
	{
	    i32 Flag0;
	    i32 Flag1;
	    i32 Flag2;
	    i32 Flag3;
	};

	struct
	{
	    i32 TextIndexPosition;
	    i32 Flag11;
	    i32 Flag22;
	    i32 Flag33;
	};
    };

    union
    {
	i32 TextureId;
    };

} SuiWidget;

typedef struct SuiDrawData
{
    i32 FontMaxHeight;

    i64 ActiveID;
    i64 HotID;

    SuiPanelId ActivePanelId;
    SuiPanelId HotPanelId;

    i32 LastFocusedID;
    v2 LastFocusedPosition;
    v2 LastFocusedSize;
    i32 LeftKeyWasPressed;

    /* i32 FocusID; */
    //i32 TextIndexPosition;
    //SuiModeType Mode;

    /* Input */
    //SuiKeyData KeyData[SuiKeys_Count];
    //i32 CharTyped;
//#define SUI_MAX_CHAR_TO_SAVE 20
//    i32 CharData[SUI_MAX_CHAR_TO_SAVE];
} SuiDrawData;

/*
	 #####################################
	       DOCS(typedef): Sui Layout
	 #####################################
*/
typedef enum SuiLayoutType
{
    SuiLayoutType_None = 0,
    SuiLayoutType_VerticalStack,
    SuiLayoutType_Count
} SuiLayoutType;

typedef struct SuiLayoutData
{
    SuiLayoutType LayoutType;
    v3 NextPosition;
    v2 NextSize;
    f32 ZOrder;
} SuiLayoutData;

void sui_set_layout(SuiLayoutType layoutType);
SuiLayoutData sui_get_layout_data(SuiLayoutType layoutType, v3 pos);
v3 sui_layout_get_next_position(SuiLayoutData* pLayoutData);

/*
	 #####################################
	       DOCS(typedef): Sui Style
	 #####################################
*/

typedef struct SuiStyleColorItem
{
    v4 Background;
    v4 Hovered;
    v4 Clicked;
} SuiStyleColorItem;

typedef struct SuiStyleSizeItem
{
    v2 Min;
    v2 Hovered;
    v2 Clicked;
} SuiStyleSizeItem;

typedef enum SuiVerticalAlignType
{
    SuiVerticalAlignType_None = 0,
    SuiVerticalAlignType_Top,
    SuiVerticalAlignType_Center,
    SuiVerticalAlignType_Bot
} SuiVerticalAlignType;

typedef enum SuiHorizontalAlignType
{
    SuiHorizontalAlignType_None = 0,
    SuiHorizontalAlignType_Left,
    SuiHorizontalAlignType_Center,
    SuiHorizontalAlignType_Right
} SuiHorizontalAlignType;

v3 sui_align_get_text_position(WideString* text, v3 widgetPosition, v2 widgetSize, SuiVerticalAlignType verticalAlign, SuiHorizontalAlignType horizontalAlign, f32 marginX);


typedef struct SuiStyleOffsetItem
{
    v2 Margin;
    v2 Padding;
    SuiVerticalAlignType VerticalAlign;
    SuiHorizontalAlignType HorizontalAlign;
} SuiStyleOffsetItem;

typedef struct SuiStyleItem
{
    SuiStyleColorItem Color;
    SuiStyleSizeItem Size;
    SuiStyleOffsetItem Offset;
} SuiStyleItem;

typedef enum SuiStyleItemType
{
    //DOCS(typedef): Panel
    SuiStyleItemType_Panel = 0,
    SuiStyleItemType_Panel_Header,
    SuiStyleItemType_Panel_Header_Close_Button,
    SuiStyleItemType_Panel_Header_Close_Button_X,
    SuiStyleItemType_Panel_Border,

    SuiStyleItemType_Widget_Button,

    SuiStyleItemType_Widget_Slider_F32,
    SuiStyleItemType_Widget_Slider_F32_Slider,

    SuiStyleItemType_Widget_Checkbox,

    SuiStyleItemType_Widget_Text,

    SuiStyleItemType_Widget_Input_String,

    SuiStyleItemType_Widget_Dropdown,

    SuiStyleItemType_Widget_Image,

    SuiStyleItemType_LayoutType_VerticalStack,

    SuiStyleItemType_Count
} SuiStyleItemType;

typedef struct SuiStyle
{
    SuiStyleItem Items[SuiStyleItemType_Count];
} SuiStyle;

void sui_style_init(SuiStyle* style, Font* pFontInfo);
v4 sui_style_get_color_by_state(SuiStyle* style, SuiStyleItemType itemType, SuiState state);
SuiStyleSizeItem sui_style_get_size(SuiStyle* style, SuiStyleItemType itemType);
SuiStyleOffsetItem sui_style_get_offset(SuiStyle* style, SuiStyleItemType itemType);
SuiStyle* sui_get_style();


/*
	 #####################################
	       DOCS(typedef): Sui Monitor
	 #####################################
*/
typedef struct SuiMonitor
{
    v2 DisplaySize;
    v2 PrevDisplaySize;
} SuiMonitor;

/*
	 #####################################
	     DOCS(typedef): Sui Frame Info
	 #####################################
*/
typedef struct SuiFrameInfo
{
    f32 Timestep;
} SuiFrameInfo;

/*
	 #####################################
	     DOCS(typedef): Infrastructure
	 #####################################
*/
i32 sui_begin_frame(SuiFrameInfo suiFrameInfo);
void sui_end_frame();
void sui_auto_layout();

/*
  DOCS(typedef): Sui Panels
*/
typedef enum SuiPanelFlags
{
    SuiPanelFlags_None          = 0,
    SuiPanelFlags_WithOutHeader = 1 << 0,
    SuiPanelFlags_Movable       = 1 << 1
} SuiPanelFlags;

typedef struct SuiPanel
{
    SuiPanelId Id;
    SuiWidgetId WidgetIndex;
    WideString* Label;
    SuiPanelFlags Flags;

    SuiLayoutType LayoutType;
    SuiLayoutData LayoutData;
    SuiWidget* Widgets;

    SuiState State;
    SuiState HeaderState;
    SuiState CloseButtonState;

    v3 Position;
    /* DOCS(typedef): If Size == {0, 0}, then auto size */
    v2 Size;

    i32 IsBlockedByOther;

} SuiPanel;

v2 sui_panel_get_header_size(SuiPanel* suiPanel);
v3 sui_panel_get_header_position(SuiPanel* pSuiPanel);
v3 sui_panel_get_header_text_position(SuiPanel* pSuiPanel);
v2 sui_panel_get_close_button_size(v2 hdrSize);
v3 sui_panel_get_close_button_position(SuiPanel* suiPanel, v2 closeButtonSize);
void sui_panel_get_close_button_x_coordinates(v2 fullSize, v3 startPos, v2 xMinSize, f32* psx, f32* psy, f32* pex, f32* pey, f32* pzv);
v3 sui_layout_process_next_position(SuiLayoutData* pSuiLayoutData, v2 widgetSize);
void sui_layout_reset_for_panels(SuiLayoutData* pSuiLayoutData, SuiPanel* pSuiPanel);
v3 sui_panel_get_widget_offset(v3 panelPos, v2 headerSize, v2 padding);
i32 sui_panel_is_active(SuiPanel* pSuiPanel);
i32 sui_panel_is_active_or_focus(SuiPanel* pSuiPanel);
SuiPanel* sui_panel_get_by_i(i32 i);
SuiPanel sui_panel_get_by_i_value(i32 i);
SuiPanel* sui_panel_get_next_tab();
i32 sui_panel_begin(WideString* label, i32* isVisible, SuiPanelFlags flags);
void sui_panel_end();

/*
	 #####################################
	     DOCS(typedef): Sui Frame Cache
	 #####################################
*/
typedef struct SuiPanelKeyValue
{
    WideString Key;
    SuiPanel Value;
} SuiPanelKeyValue;

typedef struct SuiFrameCache
{
    SuiPanelId ActivePanelId;
    SuiPanelId FocusPanelInd;
    i32 CurrentPanelInd;
    //i32* SavePanel;
    i32* Panels;// NOTE(typedef) its ok
    SuiPanelKeyValue* PanelsTable;
} SuiFrameCache;

/*
	 #####################################
	     DOCS(typedef): Sui Instance
	 #####################################
*/
typedef struct SuiInstanceCreateInfo
{
    v2 DisplaySize;
    Font* pFont;
} SuiInstanceCreateInfo;

typedef struct GlobalStates
{
    v2 NextPosition;
} GlobalStates;

typedef struct SuiInstance
{
    i32 IsInitialized;
    Font* pFont;
    SuiDrawData DrawData;
    SuiInputData InputData;
    SuiLayoutType LayoutType;
    SuiLayoutData PanelLayoutData;

    GlobalStates States;
    SuiStyle Style;
    SuiMonitor Monitor;

    // NOTE(typedef): mb safe this in file like imgui does
    SuiFrameCache PrevFrameCache;

    SuiFrameInfo FrameInfo;
} SuiInstance;

void sui_init(SuiInstanceCreateInfo createInfo);

/*
	 #####################################
	     DOCS(typedef): Sui Widgets
	 #####################################
*/
SuiState sui_button(WideString* label, v2 definedSize);
SuiState sui_slider_f32(WideString* label, f32* valuePtr, v2 minMax);
SuiState sui_checkbox(WideString* label, i32* valuePtr);
SuiState sui_text(WideString* format, ...);
SuiState sui_input_string(WideString* label, wchar* buf, i32 len, i32 maxLength);
SuiState sui_dropdown(WideString* label, wchar** texts, i32 textsCount, i32* selected);
SuiState sui_image(WideString* label, i64 textureId, v2 size, v4 color);
/*
  void* data, void* (*getValue)(void* data), void* (*getText)(void* data)
*/

f32 sui_slider_get_slide_offset_x(f32 value, v2 minMax);
v3 sui_slider_get_slide_position(v3 position, f32 offsetX);
v2 sui_slider_get_slide_size(v2 sliderSize);
void sui_checkbox_get_metrics(v3 widgetPos, v2 widgetSize, v3* checkPos, v2* checkSize);
v3 sui_input_get_text_position(WideString* text, v3 backgroundPos, v2 widgetSize);
v2 sui_input_get_text_size(v2 backgroundSize);
f32 sui_get_full_width(SuiWidget widget);

/*
	 #####################################
	     DOCS(typedef): Public
	 #####################################
*/
SuiInstance* sui_get_instance();
SuiDrawData* sui_get_draw_data();
SuiInputData* sui_get_input_data();
//SuiLayoutData* sui_get_layout_data();
SuiPanel* sui_get_current_panel();
void sui_set_current_panel(SuiPanel suiPanel);
void sui_set_next_position(v2 position);

#endif // SUIF_H
