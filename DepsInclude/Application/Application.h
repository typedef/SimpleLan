#ifndef APPLICATION_H
#define APPLICATION_H

#include <Utils/Types.h>

typedef struct SimpleWindow SimpleWindow;
typedef struct Event Event;

typedef struct Layer
{
    const char* Name;
    void (*OnAttach)();
    // NOTE(bies): Some layer want call other function after they been initialized in *_on_attach()
    void (*OnAttachFinished)();
    void (*OnUpdate)(f32 timestep);
    void (*OnEvent)(Event* event);
    void (*OnDestoy)();
    void (*OnUIRender)();
} Layer;

typedef struct ApplicationSettings
{
    i16 Width;
    i16 Height;
    i8 IsInitOpenGl;
    i8 IsInitImGui;
    i8 IsFrameRatePrinted;
    u8 UiFps;
    const char* Name;
} ApplicationSettings;

typedef struct RuntimeStatistics
{
    //DOCS(typedef): Frame time (Timestep) related
    f32 CurrentTime;
    f32 LastFrameTime;
    f32 Timestep;

    //DOCS(typedef): Second Counter
    f32 SecondElapsed;

    i32 FramesCount; //TODO()rename to fps

    // DOCS(typedef): Ui updates related
    u16 UiFps;
    u8 FpsBeforeUiUpdate;
} RuntimeStatistics;

typedef struct Application
{
    /* TODO(typedef): Make this bit flag */
    i8 IsMinimized;
    i8 IsRunning;
    i8 IsOpenGlInit;
    i8 IsInitImGui;
    i8 IsFrameRatePrinted;
    char* EventTypeToString[32];
    Layer* Layers;
    SimpleWindow* Window;

    RuntimeStatistics Stats;
} Application;

Application* application_get();
SimpleWindow* application_get_window();
void application_push_layer(Layer layer);
void application_create(ApplicationSettings appSettings);
void application_start();
void application_on_event(Event* event);
void application_close();
void application_end();

#endif
