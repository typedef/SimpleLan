#ifndef SCENE_BASIC_H
#define SCENE_BASIC_H

#include <Utils/Types.h>

typedef struct CameraComponent CameraComponent;

void scene_basic_create_grid(v4 gridColor, f32 lineThickness, f32 from, f32 to, f32 cellSize);
void scene_basic_render_grid();

void scene_basic_render_camera_orientation(CameraComponent* camera, v4 frontColor, v4 rightColor, v4 upColor);

#endif
