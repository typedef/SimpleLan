#ifndef MODEL_RENDERER_H
#define MODEL_RENDERER_H

#include <Utils/Types.h>
#include <Model/StaticModel.h>
#include <Graphics/OpenGLBase.h>

#include <EntitySystem/Components/Light/DirectionalLightComponent.h>
#include <EntitySystem/Components/Light/PointLightComponent.h>
#include <EntitySystem/Components/Light/FlashLightComponent.h>

typedef struct CameraComponent CameraComponent;

typedef struct LightsData
{
    DirectionalLightComponent* DirectionalLights;
    PointLightComponent* PointLights;
    FlashLightComponent* FlashLights;
} LightsData;

force_inline LightsData
lights_data_new(DirectionalLightComponent* directionalLights,
		PointLightComponent* pointLights,
		FlashLightComponent* flashLights)
{
    LightsData lightData = {
	.DirectionalLights = directionalLights,
	.PointLights = pointLights,
	.FlashLights = flashLights,
    };

    return lightData;
}

//NOTE(bies): Mesh can be anything, so how can we batch at least 1 mesh type
typedef struct ModelRenderer
{
    Shader Shader;
    CameraComponent* Camera;
    VertexArray VAO;
    StaticMeshVertex* Vertices;
    //u32* Indices;
    //size_t FlatDataCount;
    size_t IndicesCount;
    size_t Size;
} ModelRenderer;

#define MLN(x) ((size_t)x * 1000 * 1000)

#define R3D_MESH_VERTICES_COUNT MLN(5)
#define R3D_MESH_MAX_SIZE ((size_t)(R3D_MESH_VERTICES_COUNT * sizeof(StaticMeshVertex)))
#define R3D_MESH_INDEX_COUNT (f64(R3D_MESH_VERTICES_COUNT) / 3 + 1) * 6
#define R3D_MESH_INDEX_SIZE (R3D_MESH_INDEX_COUNT * sizeof(u32))

void renderer3d_model_init(CameraComponent* camera);
void renderer3d_model_set_camera(CameraComponent* camera);
void renderer3d_model_draw(StaticModel model, m4 transform);

#endif
