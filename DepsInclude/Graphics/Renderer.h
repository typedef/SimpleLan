#ifndef RENDERER_H
#define RENDERER_H

#include <Graphics/OpenGLBase.h>
#include <Math/SimpleMath.h>

typedef struct RuntimeCamera RuntimeCamera;
typedef struct CameraComponent CameraComponent;

/* Base Renderer */
void renderer_set_viewport(u32 width, u32 height);
void renderer_clear(v4 color);

typedef enum RendererType
{
    RendererType_Renderer = 0,
    RendererType_Renderer2D,
    RendererType_Renderer3D,
    RendererType_RendererModel
} RendererType;

void renderer_flag_type_enable(RendererType rendererType);
void renderer_flag_type_disable(RendererType rendererType);

void renderer_enable_wireframe_mode(RendererType type);
void renderer_enable_fill_mode(RendererType type);
void renderer_enable_face_culling(RendererType type);
void renderer_enable_blending(RendererType type);

void renderer_disable_face_culling(RendererType type);
void renderer_enable_depth_testing(RendererType type);
void renderer_disable_depth_testing(RendererType type);
void renderer_disable_blending(RendererType type);

/*
	 Framebuffer Rendering
*/
void renderer_init();
void renderer_flush(u32 screenTextureID);


/*
	     2D Renderer
*/
typedef struct Renderer2DStatistics
{
    //Empty for now
} Renderer2DStatistics;
void renderer2d_init(CameraComponent* camera);
void renderer2d_set_camera(CameraComponent* camera);
void renderer2d_destroy();
void renderer2d_submit(m4 transform, v4 color, u32 textureAsID);
void renderer2d_submit_rectangle(v3 position, v2 size, v4 color, u32 textureAsID);
void renderer2d_submit_rectangle_color(v3 position, v2 size, v4 color);
void renderer2d_submit_empty_rectangle(v3 position, v2 size, v4 color);

void renderer2d_submit_line(v3 start, v3 end, v4 color);

void renderer2d_set_line_thickness(f32 thickness);
void renderer2d_flush_rectangles();
void renderer2d_flush_lines();
void renderer2d_flush();
void renderer2d_draw_cursor();

/*
	     3D Renderer
*/
void renderer3d_init(RuntimeCamera* camera);
i32 renderer3d_register_texture(Texture2D newTexture, v2 textureCoords[4]);

void renderer3d_submit_cube_light(m4 transform, v4 lightColor);
void renderer3d_submit_cube(m4 transform, Texture2D front, Texture2D back, Texture2D left, Texture2D right, Texture2D bottom, Texture2D up);
void renderer3d_submit_cube_position(v3 position,
				     Texture2D front, Texture2D back,
				     Texture2D left, Texture2D right,
				     Texture2D bottom, Texture2D up);
void renderer3d_submit_craft_cube(v3 center, Texture2D up, Texture2D down, Texture2D side);


/* Flush functions */
v4* temp_renderer3d_get_light_position();
v3* temp_renderer3d_get_light_color();
void renderer3d_set_geometry();
void renderer3d_unset_geometry();
void renderer3d_flush_cubes();
void renderer3d_flush();

/* Cubemap */
void renderer3d_cubemap_init();
void renderer3d_cubemap_set_camera(CameraComponent* camera);
void renderer3d_cubemap_draw(u32 cubemapId);


#endif
