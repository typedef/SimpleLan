#ifndef UI_RENDERER_H
#define UI_RENDERER_H

#include <Utils/Types.h>
#include <Utils/Font.h>

typedef struct WideString WideString;
typedef struct CameraComponent CameraComponent;
typedef struct RuntimeCamera RuntimeCamera;

typedef struct RendererSettings
{
    size_t MaxGlyphsCount;
    size_t MaxRectsCount;
    size_t MaxLinesCount;
    CameraComponent* Camera;
    RuntimeCamera* LegacyCamera;
} RendererSettings;

CharChar GetCharChar(i32 c, i32* ind);
void SetCharChar(CharChar charChar, i32 ind);

//remove this
void ui_renderer_geometry_init(RendererSettings settings);

void ui_renderer_init(RendererSettings settings);
void ui_renderer_deinit();
void ui_renderer_draw_text_ext(void* buffer, size_t length, v3 position, v2 drawable, v4 color);
void ui_renderer_draw_text(WideString* string, v3 position, v2 drawable, v4 color);
void ui_renderer_draw_rect_ext(v2 position, v2 scale, v4 color0, v4 color1, v4 color2, v4 color3);
void ui_renderer_draw_rect(v2 position, v2 scale, v4 color);
void ui_renderer_draw_line_ext(v2 from, v2 to, v4 color0, v4 color1, f32 lineThickness);
void ui_renderer_draw_line(v2 from, v2 to, v4 color, f32 lineThickness);
void ui_renderer_flush(f32 w, f32 e);

/* Additional Helper Functions */
f32 ui_renderer_get_offset_for_current_font();

typedef struct UIRendererStatistics
{
    size_t MaxGlyphsCount;
    size_t MaxRectsCount;
    size_t MaxLinesCount;

    size_t GlyphsCount;
    size_t RectsCount;
    size_t LinesCount;
} UIRendererStatistics;
UIRendererStatistics ui_renderer_statistics_get();


#endif
