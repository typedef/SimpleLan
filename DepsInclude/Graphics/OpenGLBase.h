#ifndef OPENGLBASE
#define OPENGLBASE

#include <stdio.h>
#include <Utils/Types.h>
#include <Math/SimpleMath.h>

typedef void* (* GLADloadproc)(const char *name);
i32 opengl_context_init(GLADloadproc gladLoadProc);

typedef enum DataType
{
    Float1 = 0, Float2, Float3, Float4,
    Int1, Int2, Int3, Int4
} DataType;

force_inline u32
data_type_get_size(DataType type)
{
    switch (type)
    {
    case Float1: return 4;
    case Float2: return 4 * 2;
    case Float3: return 4 * 3;
    case Float4: return 4 * 4;
    case Int1: return 4;
    case Int2: return 4 * 2;
    case Int3: return 4 * 3;
    case Int4: return 4 * 4;
    }

    vassert(0 && "Wrong datatype!");
    return 0;
}

force_inline u32
data_type_get_count(DataType type)
{
    switch (type)
    {
    case Float1: return 1;
    case Float2: return 2;
    case Float3: return 3;
    case Float4: return 4;
    case Int1: return 1;
    case Int2: return 2;
    case Int3: return 3;
    case Int4: return 4;
    }

    vguard(0 && "No return value!");
    return 0;
}

typedef struct BufferLayout
{
    i8 IsNormalized;
    DataType Type;
    i32 Size;
    i32 Count;
    i32 Offset;
} BufferLayout;

typedef struct VertexBuffer
{
    u32 RendererID;
    i32 Stride;
    BufferLayout* Layout;
} VertexBuffer;

typedef struct IndexBuffer
{
    u32 ID;
} IndexBuffer;

typedef struct VertexArray
{
    u32 RendererID;
    VertexBuffer Vertex;
    IndexBuffer Index;
} VertexArray;


BufferLayout* buffer_layout_create(DataType types[], i32 count);
size_t buffer_layout_get_size(BufferLayout* layout);

i32 vertex_buffer_get_current();
void vertex_buffer_set_data(VertexBuffer* buffer, f32* data, u32 size);
VertexBuffer vertex_buffer_create(f32* vertices, u32 size);
VertexBuffer vertex_buffer_create_allocated(u32 size);
void vertex_buffer_add_layout(VertexBuffer* buffer, BufferLayout* layout);
void vertex_buffer_bind(VertexBuffer* buffer);
void vertex_buffer_unbind();

i32 index_buffer_get_current();
IndexBuffer index_buffer_create(u32* indices, u32 size);
IndexBuffer index_buffer_allocated(size_t size);
void index_buffer_set_data(IndexBuffer* buffer, u32* indices, size_t indicesSize);
void index_buffer_bind(IndexBuffer* buffer);
void index_buffer_unbind();

i32 vertex_array_get_current();
VertexArray vertex_array_create(VertexBuffer vertexBuffer, IndexBuffer indexBuffer);
VertexArray vertex_array_create_wo_index(VertexBuffer vertexBuffer);
void vertex_array_enable_layout(VertexArray* vertexArray);
void vertex_array_disable_layout(VertexArray* vertexArray);
void vertex_array_bind(VertexArray* va);
void vertex_array_unbind();
void vertex_array_bind_wo_index(VertexArray* vertexArray);
void vertex_array_unbind_wo_index(VertexArray* vertexArray);
void vertex_array_destroy(VertexArray* va);

typedef enum FramebufferType
{
    FramebufferType_None = 0,

    FramebufferType_Rgba, // RGBA8
    FramebufferType_RedInteger, // RED_INTEGER
    FramebufferType_Depth // DEPTH24_STENCIL8
} FramebufferType;

typedef struct FramebufferSettings
{
    u32 Width;
    u32 Height;
    FramebufferType Attachments[4];
    i32 Count;
    i32 SamplesCount;
} FramebufferSettings;

typedef struct Framebuffer
{
    i8 IsSwapChainTarget;
    i32 SamplesCount;
    u32 Width;
    u32 Height;
    u32 Samples;
    u32 RendererId;
    i32 Count;
    u32 ColorAttachments[4];
    u32 DepthAttachment;

    FramebufferType AttachmentTypes[4];
} Framebuffer;

Framebuffer framebuffer_create(FramebufferSettings settings);
void framebuffer_invalidate(Framebuffer* framebuffer, u32 width, u32 height, FramebufferType* attachments, i32 count, i32 multisampled);
i32 framebuffer_read_pixel(Framebuffer* framebuffer, i32 attachment, i32 x, i32 y);
void framebuffer_read_pixel_color(Framebuffer* framebuffer, i32 attachment, i32 x, i32 y, v4 result);
void framebuffer_bind(Framebuffer* framebuffer);
void framebuffer_multisample_blit(Framebuffer* framebuffer, Framebuffer* toDraw);
void framebuffer_unbind();
void framebuffer_transfer(u32 read, u32 draw, u32 width, u32 height);
void framebuffer_destroy(Framebuffer* framebuffer);

typedef enum TextureFilterType
{
    TextureFilterType_Linear = 0,
    TextureFilterType_Nearest
} TextureFilterType;
typedef struct Texture2DSettings
{
    i32 IsFlipOnLoad;
    TextureFilterType FilterType;
} Texture2DSettings;

typedef struct Texture2D
{
    u32 ID;
    u32 Width;
    u32 Height;
    u32 Channels;
    u32 Slot;
    u32 DataFormat;
    const char* Path;
} Texture2D;

Texture2D texture2d_create(const char* path, Texture2DSettings settings);
Texture2D texture2d_create_from_buffer(void* data, u32 width, u32 height, u8 channels, TextureFilterType filterType);
void texture2d_set_data(Texture2D* texture, void* data);
// dont do this
// void texture2d_set_from_file(Texture2D* texture, const char* path);
void texture2d_bind(Texture2D* texture, u32 slot);
void texture2d_unbind(Texture2D* texture);
void texture2d_bind_index(u32 id, u32 slot);
void texture2d_unbind_index(u32 id, u32 slot);

void texture2d_delete(Texture2D* texture);
void texture2d_destroy(Texture2D texture);

void texture2d_jpg_to_png(const char* path);

typedef struct CubeMap
{
    u32 ID;
} CubeMap;
void cubemap_texture_split(const char* loadPath);

CubeMap cube_map_create(const char* directoryPath);
CubeMap cube_map_create_from_single_file(const char* path);

typedef struct TextureAtlas
{
    f32 AtlasWidth;
    f32 AtlasHeight;
    f32 TextureWidth;
    f32 TextureHeight;
    Texture2D Texture;
} TextureAtlas;

void texture_atlas_create(TextureAtlas* atlas, const char* path, v2 atlasSize, v2 textureSize);

void shader_debug(i32 visible);
typedef struct ShaderSource
{
    const char* vertex_shader;
    const char* fragment_shader;
} ShaderSource;

typedef struct UniformTableType
{
    const char* Key;
    i32 Value;
} UniformTableType;

typedef struct Shader
{
    u32 ShaderID;
    UniformTableType* UniformTable;
} Shader;

ShaderSource shader_load(const char* shaderPath);
Shader shader_compile(ShaderSource source);
Shader shader_create(const char* shaderPath);
Shader shader_compile_safe(const char* shaderPath);
void shader_delete(Shader* shader);
void shader_bind(Shader* shader);
void shader_unbind();
void shader_delete_collection();

void shader_set_1float(Shader* shader, const char* uniformName, f32 v0);
void shader_set_2float(Shader* shader, const char* uniformName, f32 v0, f32 v1);
void shader_set_3float(Shader* shader, const char* uniformName, f32 v0, f32 v1, f32 v2);
void shader_set_4float(Shader* shader, const char* uniformName, f32 v0, f32 v1, f32 v2, f32 v3);

void shader_set_color(Shader* shader, const char* uniformName, v4 color);
void shader_set_1int(Shader* shader, const char* uniformName, i32 v0);
void shader_set_2int(Shader* shader, const char* uniformName, i32 v0, i32 v1);
void shader_set_3int(Shader* shader, const char* uniformName, i32 v0, i32 v1, i32 v2);
void shader_set_4int(Shader* shader, const char* uniformName, i32 v0, i32 v1, i32 v2, i32 v3);

void shader_set_1uint(Shader* shader, const char* uniformName, u32 v0);
void shader_set_2uint(Shader* shader, const char* uniformName, u32 v0, u32 v1);
void shader_set_3uint(Shader* shader, const char* uniformName, u32 v0, u32 v1, u32 v2);
void shader_set_4uint(Shader* shader, const char* uniformName, u32 v0, u32 v1, u32 v2, u32 v3);

void shader_set_float1(Shader* shader, const char* uniformName, i32 count, f32* values);
void shader_set_float(Shader* shader, const char* uniformName, f32 value);
void shader_set_f32(Shader* shader, const char* uniformName, f32 value);
void shader_set_float2(Shader* shader, const char* uniformName, i32 count, f32* values);
void shader_set_float3(Shader* shader, const char* uniformName, i32 count, f32* values);
void shader_set_v3(Shader* shader, const char* uniformName, v3 vector);
void shader_set_float4(Shader* shader, const char* uniformName, i32 count, f32* values);
void shader_set_v4(Shader* shader, const char* uniformName, v4 vector);
void shader_set_i32(Shader* shader, const char* uniformName, i32 value);
void shader_set_texture(Shader* shader, const char* uniformName, u32 textureID);

void shader_set_int1(Shader* shader, const char* uniformName, i32 count, i32* values);
void shader_set_int2(Shader* shader, const char* uniformName, i32 count, i32* values);
void shader_set_int3(Shader* shader, const char* uniformName, i32 count, i32* values);
void shader_set_int4(Shader* shader, const char* uniformName, i32 count, i32* values);

void shader_set_uint1(Shader* shader, const char* uniformName, i32 count, u32* values);
void shader_set_uint2(Shader* shader, const char* uniformName, i32 count, u32* values);
void shader_set_uint3(Shader* shader, const char* uniformName, i32 count, u32* values);
void shader_set_uint4(Shader* shader, const char* uniformName, i32 count, u32* values);
void shader_set_mat2(Shader* shader, const char* uniformName, i32 count, i8 transpose, f32* values);
void shader_set_m3(Shader* shader, const char* uniformName, i32 count, i8 transpose, f32* values);
void shader_set_m4(Shader* shader, const char* uniformName, i32 count, i8 transpose, f32* values);

#endif
