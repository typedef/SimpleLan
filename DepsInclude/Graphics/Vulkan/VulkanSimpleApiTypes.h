#ifndef VULKAN_SIMPLE_API_TYPES_H
#define VULKAN_SIMPLE_API_TYPES_H

#include <Utils/Types.h>
#include <Graphics/SimpleWindow.h>

typedef struct VsaSettings
{
    i32 IsDebugEnabled;
    i32 IsVSyncEnabled;
    i32 SamplesCount;
} VsaSettings;

/*
  DOCS(typedef): Structs
*/
typedef struct VsaQueueFamily
{
    i32 GraphicsIndex;
    i32 PresentationIndex;
} VsaQueueFamily;

typedef struct VsaSwapChainSettings
{
    VkSurfaceFormatKHR SurfaceFormat;
    VkPresentModeKHR PresentationMode;
    v2 Size;
} VsaSwapChainSettings;

typedef struct VsaImageCreateExtSettings
{
    i32 Width;
    i32 Height;
    i32 Depth;
    i32 MipLevels;
    VkSampleCountFlagBits SamplesCount;
    VkFormat Format;
    VkImageTiling ImageTiling;
    VkImageUsageFlags ImageUsageFlags;
    VkMemoryPropertyFlags MemoryPropertyFlags;
} VsaImageCreateExtSettings;

typedef enum VsaBufferType
{
    VsaBufferType_Static = 0,
    VsaBufferType_Dynamic,
} VsaBufferType;

typedef struct VsaBuffer
{
    VsaBufferType Type;
    VkBuffer Staging;
    VkBuffer Gpu;
    VkDeviceMemory StagingMemory;
    VkDeviceMemory GpuMemory;
    u64 Offset;
} VsaBuffer;


typedef struct VsaTextureImageSettings
{
    const char* Path;
    i32 MipLevels;
    i32 Height;
    i32 Width;
    i32 Channels;
    void* Data;
} VsaTextureImageSettings;

typedef struct VsaTexture
{
    /* DOCS(typedef): AssetManager Ind */
    i64 Id;
    VkImage Image;
    VkDeviceMemory ImageMemory;
    VkImageView ImageView;
    VkFormat Format;
    VkSampler Sampler;
    i32 MipLevels;
    char* Name;
} VsaTexture;

/*
  DOCS(typedef): 0 - no texture, all other number is valid texture Id
*/
typedef i64 VsaTextureId;

/*

  DOCS(typedef): SHADERS PART BEGIN

*/
typedef struct VsaShaderPaths
{
    const char* VertexPath;
    const char* FragmentPath;
    const char* GeometryPath;
    const char* ComputePath;
    const char* TesselationControlPath;
    const char* TesselationEvaluationPath;
} VsaShaderPaths;

typedef enum VsaShaderType
{
    VsaShaderType_Vertex = 0,
    VsaShaderType_Fragment,
    VsaShaderType_Geometry,
    VsaShaderType_Compute,
    VsaShaderType_TesselationControl,
    VsaShaderType_TesselationEvaluation,
    VsaShaderType_Count,
} VsaShaderType;

typedef enum VsaShaderCompilationResult
{
    VsaShaderCompilationResult_FileNotExist = 0,
    VsaShaderCompilationResult_AlreadyCompiled,
    VsaShaderCompilationResult_CompilationError,
    VsaShaderCompilationResult_InvalidStage,
    VsaShaderCompilationResult_InternalError,
    VsaShaderCompilationResult_NullResultObject,
    VsaShaderCompilationResult_ValidationError,
    VsaShaderCompilationResult_TransformationError,
    VsaShaderCompilationResult_ConfigurationError,
    VsaShaderCompilationResult_Successed
} VsaShaderCompilationResult;

typedef struct VsaShaderCompiledBytecode
{
    u32* Bytecode;
    size_t Size;
} VsaShaderCompiledBytecode;

typedef struct VsaShaderOutput
{
    VsaShaderCompiledBytecode Vertex;
    VsaShaderCompiledBytecode Fragment;
    VsaShaderCompiledBytecode Geometry;
    VsaShaderCompiledBytecode Compute;
    VsaShaderCompiledBytecode TesselationControl;
    VsaShaderCompiledBytecode TesselationEvaluation;
} VsaShaderOutput;

typedef struct VsaShaderAttribute
{
    VkFormat Format;
    i32 Offset;
} VsaShaderAttribute;

typedef struct VsaShaderDescriptor
{
    VkDescriptorType Type;
    i32 Binding;
    i64 Count;
    size_t Size;
} VsaShaderDescriptor;

typedef struct VsaUniformInternal
{
    /*char* Name;*/
    i32 Binding;
    i32 Count;
    size_t ElementSize;
    size_t Size;
    VkBuffer Buffer;
    VkDeviceMemory DeviceMemory;
    void* MappedMemory;
    size_t Offset;
} VsaUniformInternal;

typedef struct VsaShaderStage
{
    VsaShaderType Type;
    const char* ShaderSourcePath;
    VsaShaderDescriptor Descriptors[15];
    i32 DescriptorsCount;
} VsaShaderStage;

typedef struct VsaPushConstant
{
    // VK_SHADER_STAGE_VERTEX_BIT
    VkShaderStageFlags ShaderStage;
} VsaPushConstant;

typedef struct VsaShaderBindingsCore
{
    //i32 TextureDescriptorsCount;
    VsaShaderDescriptor* Descriptors;
    VsaUniformInternal* Uniforms;
    //VsaPushConstant* PushContants;
    VkShaderStageFlags PushConstantStageFlag; //VK_SHADER_STAGE_VERTEX_BIT
    VkDescriptorSetLayoutBinding* Bindings;
    VkDescriptorSet DescriptorSet;
    VkDescriptorSetLayout DescriptorSetLayout;
    VkDescriptorPool DescriptorPool;

    // DOCS(typedef): Texture related
    i32 MaxTexturesCount;
    i32 CurrentTexturesCount;
    VkDescriptorImageInfo* ImageInfos;
    VkDescriptorImageInfo* SamplerInfos;

} VsaShaderBindingsCore;

/*
  DOCS(typedef): SHADERS PART END
*/

typedef struct VsaPipelineDescription
{
    i32 Stride;
    i32 StagesCount;
    i32 AttributesCount;
    VkPolygonMode PolygonMode;
    size_t ArenaDefaultSize;
    VsaShaderStage* Stages;
    VsaShaderAttribute* Attributes;

    size_t PustConstantSize;
    VkShaderStageFlags PushConstantStageFlag; //VK_SHADER_STAGE_VERTEX_BIT
} VsaPipelineDescription;

typedef struct VsaUnifomData
{
    void* Data;
    size_t Size;
} VsaUniformData;

typedef struct VsaPushConstantData
{
    void* Data;
    size_t Size;
} VsaPushConstantData;

typedef struct VsaDrawCall
{
    /*
      DOCS(typedef): Vertex/Index buffers related
    */
    u64 VerticesOffset;
    u64 VerticesSize; // DOCS(typedef): Needed for pipeline_add_d*_c*() func
    u64 IndicesOffset;
    u64 IndicesCount;

    u64 VerticesCount;

    /*
      DOCS(typedef): Contains one/multiple push constant data for frame
    */
    VsaPushConstantData PushConstantData;

} VsaDrawCall;

typedef struct VsaPipeline
{
    i32 IsPipelineRenderable;

    size_t ArenaDefaultSize;
    Arena** Arenas;

    VkPipeline Pipeline;
    VkPipelineLayout PipelineLayout;
    //VsaShaderDescriptor* Descriptors;
    VsaShaderBindingsCore BindingCore;

    u64 VerticesOffset;
    u64 IndicesOffset;
    VsaDrawCall* DrawCalls;
    u32 CurrentVertexId;
    u32 CurrentIndexId;

#if 1
    VsaBuffer Vertex;
    VsaBuffer Index;
#else
    VsaBuffer* VertexBuffers;
    VsaBuffer* IndexBuffers;
#endif

    VkCommandBuffer CmdBuffer;
} VsaPipeline;


#endif // VULKAN_SIMPLE_API_TYPES_H
