#ifndef VULKAN_SIMPLE_API_HELPER_H
#define VULKAN_SIMPLE_API_HELPER_H

/*
  DOCS(typedef): Structs
*/
typedef struct VkExtFunctions
{
    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT;
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT;

} VkExtFunctions;

typedef struct VkSwapChainSupportDetails
{
    VkSurfaceCapabilitiesKHR SurfaceCapabilities;
    VkSurfaceFormatKHR* SurfaceFormats;
    VkPresentModeKHR* PresentModes;
} VkSwapChainSupportDetails;

/*
  DOCS(typedef): Global variables|state
*/
static VkExtFunctions VkExts;

#define VsaNoTimeout U64_MAX
#define VsaNoFlags 0

/*
  DOCS(typdef): preprocessor part
*/
#if defined(ENGINE_DEBUG)
#define vkValidHandle(handle)						\
    ({									\
	if (handle == VK_NULL_HANDLE)					\
	{								\
	    GERROR("Handle: %s is invalid!\n", #handle);		\
	    vassert(handle != VK_NULL_HANDLE && "Invalid handle!");	\
	}								\
									\
    })
#else
#define vkValidHandle(handle)
#endif

/*
  DOCS(typedef): Functions part
*/

static void
vkPrintVersion(u32 version)
{
    printf("Version: %d.%d.%d", VK_API_VERSION_MAJOR(version), VK_API_VERSION_MINOR(version), VK_API_VERSION_PATCH(version));
}

static void
vkPrintVersionLine(u32 version)
{
    vkPrintVersion(version);
    printf("\n");
}

static void
vkPrintLayerProperty(VkLayerProperties layer)
{
    GINFO("[Layer] {\n\t.layerName = %s\n\t.Version = ", layer.layerName);
    vkPrintVersion(layer.specVersion);
    printf("\n\t.implementationVersion = ");
    vkPrintVersion(layer.implementationVersion);
    printf("\n\t.description = %s\n}\n", layer.description);
}

static void
vkPrintLayerProperties(i32 layersCount, VkLayerProperties* layersProperties)
{
    GINFO("Layers Count: %d\n", layersCount);
    for (i32 i = 0; i < layersCount; ++i)
    {
	VkLayerProperties layerProperty = layersProperties[i];
	vkPrintLayerProperty(layerProperty);
    }
}

/*
  NOTE(typedef): mb we'll need this for RenderDoc debugging
*/
static const char* const*
vkGetNeededLayers()
{
    u32 layersCount = 0;
    VkResult enumerateLayersResult =
	vkEnumerateInstanceLayerProperties(&layersCount, NULL);
    if (enumerateLayersResult != VK_SUCCESS)
    {
	GERROR("Can't get instance layer properties\n");
	vassert_break();
    }

    VkLayerProperties* layersProperties = NULL;
    array_reserve(layersProperties, layersCount);
    enumerateLayersResult =
	vkEnumerateInstanceLayerProperties(&layersCount, layersProperties);

    vkPrintLayerProperties(layersCount, layersProperties);

    char** enabledLayers = NULL;
    array_foreach(
	layersProperties,
	if (string_compare(item.layerName, "VK_LAYER_RENDERDOC_Capture"))
	{
	    GINFO("Enable layer: %s\n", item.layerName);
	    array_push(enabledLayers, item.layerName);
	});

    array_free(layersProperties);

    return (const char* const *)enabledLayers;
}

static const char* const*
vkGetExtensions()
{
    i32 requiredExtsCount;
    char** requiredExts = window_get_required_exts(&requiredExtsCount);
#if ENGINE_DEBUG
    array_push(requiredExts, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    GINFO("Get exts count: %d\n", requiredExtsCount);
    printf(MAGNETA("[Required Ext]"));
    array_foreach(requiredExts, printf("  %s", item););
    printf("\n");

    return (const char* const*) requiredExts;
}

static void
vkCheckExtSupport(const char* const* requiredExts)
{
    //vkEnumerateInstanceLayerProperties
    i32 extsCount;
    vkEnumerateInstanceExtensionProperties(NULL, &extsCount, NULL);
    VkExtensionProperties* exts = NULL;
    array_reserve(exts, extsCount);
    vkEnumerateInstanceExtensionProperties(NULL, &extsCount, exts);
    array_header(exts)->Count = extsCount;

    // array_foreach(exts, GLOG("%s\n", item.extensionName););

    i32 i, count = array_count(requiredExts);
    for (i = 0; i < count; ++i)
    {
	const char* rext = requiredExts[i];
	i32 exist = 0;
	for (i32 e = 0; e < extsCount; ++e)
	{
	    VkExtensionProperties ext = exts[e];
	    //GLOG("Extension : %s\n", ext.extensionName);
	    if (string_compare(rext, ext.extensionName))
	    {
		exist = 1;
		break;
	    }
	}

	if (!exist)
	{
	    GERROR("Extension not supported: %s\n", rext);
	    vassert(exist && "Extension not supported!");
	}
    }
}

static const char*
vkResultToString(VkResult result)
{
    switch (result)
    {

    case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";

    case VK_SUCCESS: return "VK_SUCCESS";
    case VK_NOT_READY: return "VK_NOT_READY";
    case VK_TIMEOUT: return "VK_TIMEOUT";
    case VK_EVENT_SET: return "VK_EVENT_SET";
    case VK_EVENT_RESET: return "VK_EVENT_RESET";
    case VK_INCOMPLETE: return "VK_INCOMPLETE";
    case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";

    case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
    case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
    case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
    case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
    case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
    case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
    case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
    case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
    case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
    case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
    case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";

    case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
    case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
    case VK_PIPELINE_COMPILE_REQUIRED: return "VK_PIPELINE_COMPILE_REQUIRED";
    case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
    case VK_SUBOPTIMAL_KHR: return "K_SUBOPTIMAL_KHR";
    case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
    case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
    case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
    case VK_ERROR_NOT_PERMITTED_KHR: return "VK_ERROR_NOT_PERMITTED_KHR";
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";

    case VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
    case VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
    case VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
    case VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
    case VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "VK_ERROR_COMPRESSION_EXHAUSTED_EXT";

    }

    return "Unknown result! Mb update is required for vkResultToString";
}

static VkBool32
vkDebugMessenger(VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity, VkDebugUtilsMessageTypeFlagsEXT                  messageTypes, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData)
{
    if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
    {
	printf(MAGNETA("[VULKAN INFO]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
	printf(YELLOW("[VULKAN WARNING]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
    {
	printf(RED("[VULKAN ERROR] "));
    }

    printf("%s \n", pCallbackData->pMessage);
}

static void
vkPrintError(VkResult result, const char* message)
{
    GERROR("Error aquired: %s, message: %s\n", vkResultToString(result), message);
}

#define vkValidResult(result, message)		\
    ({						\
	if (result != VK_SUCCESS)		\
	{					\
	    vkPrintError(result, message);	\
	    vguard(result == VK_SUCCESS);	\
	}					\
    })

static VkDebugUtilsMessengerCreateInfoEXT
vkCreateDebugUtilsMessengerCreateInfo()
{
    VkDebugUtilsMessageSeverityFlagsEXT severity = /* VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | */
	VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    VkDebugUtilsMessageTypeFlagsEXT messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    VkDebugUtilsMessengerCreateInfoEXT debugUtilsCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
	.flags = 0,
	.messageSeverity = severity,
	.messageType = messageType,
	.pfnUserCallback = vkDebugMessenger,
	.pUserData = NULL
    };

    return debugUtilsCreateInfo;
}

static VkDebugUtilsMessengerEXT
vkCreateDebugUtilsMessenger(VkInstance vkInstance, VkDebugUtilsMessengerCreateInfoEXT debugUtilsCreateInfo, VkAllocationCallbacks* alloc)
{
    vassert_not_null(VkExts.vkCreateDebugUtilsMessengerEXT);

    VkDebugUtilsMessengerEXT result;
    VkResult createDebugMessengerResult =
	VkExts.vkCreateDebugUtilsMessengerEXT(vkInstance, &debugUtilsCreateInfo, alloc, &result);
    if (createDebugMessengerResult != VK_SUCCESS)
    {
	vkPrintError(createDebugMessengerResult, "Can't create debug utils");
	vassert_break();
    }

    return result;
}
static void
vkDestroyDebugUtilsMessenger(VkInstance vkInstance, VkDebugUtilsMessengerEXT debugUtils, VkAllocationCallbacks* alloc)
{
    VkExts.vkDestroyDebugUtilsMessengerEXT(vkInstance, debugUtils, alloc);
}

static void
vkLoadExtsFunctions(VkInstance vkInstance)
{
    VkExts.vkCreateDebugUtilsMessengerEXT  =
	(PFN_vkCreateDebugUtilsMessengerEXT)
	vkGetInstanceProcAddr(vkInstance, "vkCreateDebugUtilsMessengerEXT");
    VkExts.vkDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)
	vkGetInstanceProcAddr(vkInstance, "vkDestroyDebugUtilsMessengerEXT");

    vassert(VkExts.vkCreateDebugUtilsMessengerEXT != NULL && "Can't load vkCreateDebugUtilsMessengerEXT");
    vassert(VkExts.vkDestroyDebugUtilsMessengerEXT != NULL && "Can't load vkDestroyDebugUtilsMessengerEXT");
}

static const char*
vkPhysicalDeviceTypeToString(VkPhysicalDeviceType type)
{
    switch (type)
    {
    case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "Other";
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "Integrated GPU";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "GPU";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "Virtual GPU";
    case VK_PHYSICAL_DEVICE_TYPE_CPU: return "CPU";
    }

    vassert_break();
    return "";
}

static void
vkPrintPhysicalDeviceProperties(VkPhysicalDeviceProperties props)
{
    printf("[Physical Device] { \n\t.apiVersion = ");
    vkPrintVersion(props.apiVersion);
    printf(",\n\t.driverVersion = ");
    vkPrintVersion(props.driverVersion);
    printf(",\n\t.vendorID = %d,\n\t.deviceID = %d,\n\t.deviceType = %s,\n\t.deviceName = %s\n}\n", props.vendorID, props.deviceID, vkPhysicalDeviceTypeToString(props.deviceType), props.deviceName);
}

static char*
vkExtent2DToString(VkExtent2D ext)
{
    char buf[61];
    string_format(buf, "width: %d height: %d", ext.width, ext.height);
    return string(buf);
}

static void
vkPrintSurfaceCapabilities(VkSurfaceCapabilitiesKHR surfaceCapabilities)
{
    Arena* arena = arena_create_and_set(KB(1));

    printf(MAGNETA("[VkSurfaceCapabilitiesKHR]"));
    printf("\n{\n\t.minImageCount = %d,", surfaceCapabilities.minImageCount);
    printf("\n\t.maxImageCount = %d [%s],",
	   surfaceCapabilities.maxImageCount,
	   (surfaceCapabilities.maxImageCount == 0) ? "Unlimited" : "Limited");
    printf("\n\t.currentExtent = %s,",
	   vkExtent2DToString(surfaceCapabilities.currentExtent));
    printf("\n\t.minImageExtent = %s",
	   vkExtent2DToString(surfaceCapabilities.minImageExtent));
    printf("\n\t.maxImageExtent = %s",
	   vkExtent2DToString(surfaceCapabilities.maxImageExtent));
    printf("\n\t.maxImageArrayLayers = %d", surfaceCapabilities.maxImageArrayLayers);

    printf("\n}");

    /* VkSurfaceTransformFlagsKHR       supportedTransforms; */
    /* VkSurfaceTransformFlagBitsKHR    currentTransform; */
    /* VkCompositeAlphaFlagsKHR         supportedCompositeAlpha; */
    /* VkImageUsageFlags                supportedUsageFlags; */

    arena_destroy(arena);
}

static void
vkPrintSurfaceCapabilitiesLine(VkSurfaceCapabilitiesKHR surfaceCapabilities)
{
    vkPrintSurfaceCapabilities(surfaceCapabilities);
    printf("\n");
}

static VkSwapChainSupportDetails
vkGetSwapChainSupportDetails(VkPhysicalDevice pDevice, VkSurfaceKHR surface)
{
    VkSwapChainSupportDetails supportDetails = {
	.SurfaceFormats = NULL,
	.PresentModes = NULL
    };

    VkResult getSurfaceCapabilitiesResult =
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
	    pDevice,
	    surface,
	    &supportDetails.SurfaceCapabilities);
    vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);

    // NOTE(typdef): get surface formats
    VkSurfaceFormatKHR* SurfaceFormats = NULL;
    i32 formatsCount;
    VkResult getSurfaceFormatResult =
	vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, NULL);
    array_reserve(supportDetails.SurfaceFormats, formatsCount);
    array_header(supportDetails.SurfaceFormats)->Count = formatsCount;
    getSurfaceFormatResult =
	vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, supportDetails.SurfaceFormats);
    vkValidResult(getSurfaceFormatResult, "Failed to get Surface Format Result \n");

    // NOTE(typedef): get surface modes
    i32 surfaceModesCount;
    VkResult getSurfaceModeResult =
	vkGetPhysicalDeviceSurfacePresentModesKHR(
	    pDevice,
	    surface,
	    &surfaceModesCount,
	    NULL);
    array_reserve(supportDetails.PresentModes, surfaceModesCount);
    array_header(supportDetails.PresentModes)->Count = surfaceModesCount;
    getSurfaceModeResult =
	vkGetPhysicalDeviceSurfacePresentModesKHR(
	    pDevice,
	    surface,
	    &surfaceModesCount,
	    supportDetails.PresentModes);
    vkValidResult(getSurfaceModeResult, "Can;t get Surface Modes!");

    return supportDetails;
}

VkImage*
vkGetSwapChainImages(VkDevice device, VkSwapchainKHR swapChain)
{
    VkImage* images = NULL;

    u32 imagesCount;
    VkResult getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapChain, &imagesCount, NULL);
    array_reserve(images, imagesCount);
    array_header(images)->Count = imagesCount;
    getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapChain, &imagesCount, images);
    vkValidResult(getSwapChainImagesResult, "Can't get swap chain images!");

    return images;
}

f32
vkGetMaxAnisotropy(VkPhysicalDevice vkPhysicalDevice)
{
    VkPhysicalDeviceProperties properties;
    vkGetPhysicalDeviceProperties(vkPhysicalDevice, &properties);
    return properties.limits.maxSamplerAnisotropy;
}

#endif //VULKAN_SIMPLE_API_HELPER_H
