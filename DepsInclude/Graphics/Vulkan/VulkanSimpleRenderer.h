#ifndef VULKAN_SIMPLE_RENDERER_H
#define VULKAN_SIMPLE_RENDERER_H

#include "VulkanSimpleApi.h"
#include <Utils/Font.h>
#include <Utils/Types.h>
#include <Model/StaticModel.h>
#include <Graphics/Light/Light.h>

/*
  DOCS(typedef): Base
*/
void vsr_add_pipeline(VsaPipeline* pVsaPipeline);
void vsr_frame_begin();
void vsr_draw_all();
void vsr_frame_end();
i32 vsr_get_image_index();

/*
  DOCS(typedef): 2d renderer
*/

/* DOCS(typedef): Helpful utils */
typedef struct VsaFont
{
    Font Font;
    VsaTexture Atlas;
} VsaFont;

VsaFont vsa_font_info_create(const char* path, i32 size, i32 isSaveBitmap);

//2d only
typedef struct Vertex2D
{
    v3 Position;
    v4 Color;
    v2 Uv;
    /*
      TODO(typedef): TextureInd can be used for
      NoTexture = -1,
      IsCircle = -2
    */
    i32 TextureInd;
    i32 IsFont;
} Vertex2D;

typedef struct Renderer2dDrawData
{
    i64 IndicesToDraw;
    Vertex2D* Vertices;
} Renderer2dDrawData;

void renderer2d_draw_data_copy(Renderer2dDrawData* this, Renderer2dDrawData* toCopy);

typedef struct Renderer2dSettings
{
    i64 MaxCount;
    VsaFont Font;
    struct Camera* pCamera;
} Renderer2dSettings;

typedef struct Renderer2d
{
    i8 IsDataSubmited;
    i32 MaxTexturesCount;
    i64 MaxCount;
    VsaTexture* pTextures;
    VsaFont CurrentFontInfo;
    Renderer2dDrawData Current;
    Renderer2dDrawData Prev;
    VsaPipeline Pipeline;
    struct Camera* pCamera;
    u32* Indices;
} Renderer2d;

void vsr_2d_create(Renderer2d* renderer, const Renderer2dSettings* settings);
void vsr_2d_submit(Renderer2d* renderer, v3 position, v2 size, v4 color, VsaTexture vsaTexture);
void vsr_2d_submit_rect(Renderer2d* renderer, v3 position, v2 size, v4 color);
void vsr_2d_submit_line(Renderer2d* renderer, v3 start, v3 end, f32 thickness, v4 color);
void vsr_2d_submit_empty_rect(Renderer2d* renderer, v3 position, v2 size, f32 thickness, v4 color);
void vsr_2d_submit_text_ext(Renderer2d* renderer, void* buffer, size_t length, i32 scale, v3 position, v2 drawable, v4 color);
void vsr_2d_submit_circle_ext(Renderer2d* renderer, v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier);
void vsr_2d_flush(Renderer2d* renderer, i32 imageIndex);
void vsr_2d_destroy(Renderer2d* renderer);


/*
  DOCS(typedef): Tests
*/
void vsr_test_submit_circle_and_line(Renderer2d* renderer, VsaTexture someTexture);

/*
  DOCS(typedef): 3d renderer
*/

typedef struct LightDataUbo
{
    i32 DirectionalLightsCount;
    DirectionalLight DirectionalLights[2];
    i32 PointLightsCount;
    PointLight PointLights[16];
    i32 FlashLightsCount;
    FlashLight FlashLights[16];
} LightDataUbo;

typedef struct ViewPositionUbo
{
    v3 Position;
} ViewPositionUbo;

typedef struct Renderer3dStaticSettings
{
    i64 MaxCount;
    struct Camera* pCamera;
} Renderer3dStaticSettings;

//TODO(typedef): Rename to VertexStatic3D
typedef struct Vertex3D
{
    v3 Position;
    v3 Normal;
    v2 Uv;
} Vertex3D;

typedef struct MeshMaterialInfo
{
    /* DOCS(typedef): Index inside Renderer3dStatic array */
    Align(16) i32 BaseColorTextureId;
    /*More field to come*/
} MeshMaterialInfo;

typedef struct Renderer3dDrawData
{
    i64 IndicesToDraw;
    Vertex3D* Vertices;
} Renderer3dDrawData;

typedef struct Renderer3dStatic
{
    i8 IsDataSubmited;
    i64 InstanceCount;
    i64 MaxCount;
    VsaPipeline Pipeline;
    struct Camera* pCamera;

    StaticModel* ModelsToDraw;
    MeshMaterialInfo* MaterialInfos;
    VsaTexture* Textures;
} Renderer3dStatic;

void vsr_3d_static_create(Renderer3dStatic* renderer, const Renderer3dStaticSettings* settings);
void vsr_3d_static_destroy(Renderer3dStatic* pRenderer);

void vsr_3d_static_draw_model(Renderer3dStatic* pRenderer, StaticModel model);
void vsr_3d_flush(Renderer3dStatic* pRenderer, i32 imageIndex, LightDataUbo lightData, v3 viewPosition);

#endif // VULKAN_SIMPLE_RENDERER_H
