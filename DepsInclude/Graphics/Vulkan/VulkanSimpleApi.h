#ifndef VULKAN_SIMPLE_API_H
#define VULKAN_SIMPLE_API_H

#include <Utils/Types.h>
#include <Graphics/SimpleWindow.h>
#include "VulkanSimpleApiTypes.h"

void vsa_set_window_resized();

VsaBuffer vsa_buffer_new(VsaBufferType type, void* data, size_t size, VkBufferUsageFlagBits usage);
void vsa_buffer_set_data(VsaBuffer vsaBuffer, void* data, size_t offset, size_t size);
void vsa_buffer_destroy(VsaBuffer vsaBuffer);

/*

  DOCS(typedef): SHADERS PART BEGIN

*/
VkShaderStageFlagBits vsa_shader_type_to_stage_flag(VsaShaderType type);
const char* vsa_shader_type_to_string(VsaShaderType type);
void vsa_shader_bindings_destroy(VsaShaderBindingsCore vsaShaderBindingsCore);
void vsa_shader_output_free(VsaShaderOutput vsaOut);

/*
  DOCS(typedef): SHADERS PART END
*/

/*
  DOCS(typedef): Pipeline related
*/

// DOCS(typedef): Should call it in order
Arena* vsa_draw_call_get_arena(VsaPipeline* pVsaPipeline, size_t size);
void vsa_draw_call_add_push_constant(VsaPipeline* pVsaPipeline, VsaDrawCall* pVsaDrawCall, void* data, size_t size);
void vsa_draw_call_bind_push_constant(VsaPipeline* pVsaPipeline, VsaDrawCall vsaDrawCall);

VsaPipeline vsa_pipeline_create(VsaPipelineDescription descriptions);
void vsa_pipeline_set_buffers(VsaPipeline* vsaPipeline, VsaBuffer vertex, VsaBuffer index);
void vsa_pipeline_bind_texture(VsaPipeline* pVsaPipeline, VsaTexture vsaTexture, i32 binding);
void vsa_pipeline_bind_textures(VsaPipeline* vsaPipeline, VsaTexture* textures, i32 count, i32 maxCount);
void vsa_pipeline_update_uniforms(VsaPipeline* pVsaPipeline);
void vsa_pipeline_update_descriptors(VsaPipeline* pVsaPipeline);
void vsa_pipeline_add_draw_call(VsaPipeline* pVsaPipeline, VsaDrawCall vsaDrawCall);
void vsa_pipeline_reset(VsaPipeline* pVsaPipeline);
void vsa_pipeline_destroy(VsaPipeline vsaPipeline);

void vsa_render_pass_proccess(VsaPipeline** pipelines, i32 pipelinesCount, i32 imageIndex);

/*#######################
  DOCS(typedef): Vsa Api
  #######################*/
void vsa_core_init(VsaSettings vsaSettings);
void vsa_core_deinit();
void vsa_swapchain_objects_recreate();
void vsa_device_wait_idle();
void vsa_local_frame_start(i32* imageIndex);
void vsa_local_frame_end(i32* imageIndex);
i32 vsa_get_max_texture_count();
i32 vsa_get_min_uniform_size();
u32 vsa_get_max_vertex_binding_in_buffer();

/*#######################
  DOCS(typedef): Textures
  #######################*/
VsaTexture vsa_texture_new(const char* path);
i32 vsa_texture_is_valid(VsaTexture vsaTexture);
void vsa_texture_destroy(VsaTexture vsaTexture);

/*#######################
  DOCS(typedef): Uniforms
  #######################*/
VsaUniformInternal vsa_uniform_create(size_t size, size_t elementSize, i32 binding);
i32 vsa_uniform_set_data(VsaUniformInternal* pVsaUniform, void* data, size_t size);
void vsa_uniform_destroy(VsaUniformInternal vsaUniform);


#endif // VULKAN_SIMPLE_API_H
