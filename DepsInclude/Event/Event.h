#ifndef EVENT_H
#define EVENT_H

#include "Utils/Types.h"
#include <InputSystem/KeyCodes.h>

/*
  TODO(typedef): Refactor this enum
*/
typedef enum EventType
{
    EventType_None = 0,

    /* DOCS(typedef): Window event */
    EventType_WindowShouldBeClosed,
    EventType_WindowResized,
    EventType_WindowFocused,
    EventType_WindowUnfocused,
    EventType_WindowMoved,
    EventType_WindowMinimized,
    EventType_WindowMaximized,
    EventType_WindowRestored,

    /* DOCS(typedef): App event */
    EventType_AppTick,
    EventType_AppUpdate,
    EventType_AppRender,

    /* DOCS(typedef): Key event */
    EventType_KeyPressed,
    EventType_KeyRepeatPressed,
    EventType_KeyRealeased,
    EventType_KeyTyped,

    EventType_CharTyped,

    /* DOCS(typedef): Mouse event */
    EventType_MouseButtonPressed,
    EventType_MouseButtonReleased,
    EventType_MouseMoved,
    EventType_MouseScrolled,

    EventType_Count
} EventType;

typedef enum EventCategory
{
    NoneCategory = 1 << 0,
    EventCategory_Window = 1 << 1,
    EventCategory_App = 1 << 2,
    EventCategory_Key = 1 << 3,
    EventCategory_Mouse = 1 << 4
} EventCategory;

typedef struct Event
{
    i8 IsHandled;
    EventType Type;
    EventCategory Category;
} Event;

typedef struct KeyPressedEvent
{
    Event Base;
    i32 KeyCode;
    i32 ScanCode;
    ActionType Action;
    ModType Modificator;
    i32 RepeatCount;
} KeyPressedEvent;

typedef struct CharEvent
{
    Event Base;
    u32 Char;
} CharEvent;

typedef struct KeyReleasedEvent
{
    Event Base;
    i32 KeyCode;
} KeyReleasedEvent;

typedef struct WindowResizedEvent
{
    Event Base;
    u32 Width;
    u32 Height;
} WindowResizedEvent;

typedef struct MouseMovedEvent
{
    Event Base;
    f32 X;
    f32 Y;
} MouseMovedEvent;

typedef struct MouseScrolledEvent
{
    Event Base;
    f64 XOffset;
    f64 YOffset;
} MouseScrolledEvent;

typedef struct MouseButtonEvent
{
    Event Base;
    i32 MouseCode;
    i32 Action;
    i32 Modificator;
} MouseButtonEvent;

// NOTE(vez): for this type event don't use struct
// only Event .Type =  WindowClosedEvent

#endif
