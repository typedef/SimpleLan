#ifndef EDITOR_VIEWPORT_H
#define EDITOR_VIEWPORT_H

#include <Utils/Types.h>

typedef struct CameraComponent CameraComponent;
typedef struct Event Event;

typedef struct EditorViewportUpdate
{
    CameraComponent* Camera;
    f32 Timestep;
} EditorViewportUpdate;

void editor_viewport_activate();
void editor_viewport_disable();
void editor_viewport_switch();
void editor_viewport_enable_movement();
void editor_viewport_disable_movement();

void editor_viewport_on_update(EditorViewportUpdate evUpdate);
void editor_viewport_on_event(Event* event);


#endif // EDITOR_VIEWPORT_H
