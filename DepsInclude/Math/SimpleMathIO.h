#ifndef SIMPLE_MATH_IO_H
#define SIMPLE_MATH_IO_H

#include "SimpleMath.h"

void _v2_print(v2 position, const char* accuracy);
void _v3_print(v3 position, const char* accuracy);
void _v4_print(v4 position, const char* accuracy);

#define v2_printa(p, a) _v2_print(p, BRIGHTWHITE("%0."#a"f"));
#define v2_print(p) v2_printa(p, 2)
#define v3_printa(p, a) _v3_print(p, BRIGHTWHITE("%0."#a"f"))
#define v3_print(p) v3_printa(p, 2)
#define v4_printa(p, a) _v4_print(p, BRIGHTWHITE("%0."#a"f"))
#define v4_print(p) v4_printa(p, 2)

/*

  ###################################
  ###################################
  M4PrintIO.h
  ###################################
  ###################################

*/
void m4_print(m4 m, const char* name);
#define M4_PRINT(m) m4_print(m, #m)


#endif
