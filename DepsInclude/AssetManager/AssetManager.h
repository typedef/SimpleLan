#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include <Utils/Types.h>
#include <Graphics/Vulkan/VulkanSimpleApiTypes.h>

typedef enum AssetType
{
    AssetType_None = 0,
    AssetType_Texture,
    AssetType_Texture_WithRect,
    AssetType_Count
} AssetType;

//typedef struct String String;

typedef struct TexturesMapTable
{
    const char* Key;
    i64 Value;
} TexturesMapTable;

// TODO(bies): const char* -> WideString from the start
// GlobalPath is Application path, all path's in game is ascii friendly so,
// we don't need impliment WideStirng Path API here
typedef struct AssetManager
{
    i32 ID;
    AssetType Type;
    const char* Name;
    const char* GlobalPath;

    VsaTexture* pTextures;
    TexturesMapTable* pTexturesTable;

    //String* Name;
    //String* GlobalPath;
} AssetManager;

void asset_manager_create();

void* asset_manager_load_asset(AssetType type, const char* diskPath);
i64 asset_manager_load_asset_id(AssetType type, const char* diskPath);
void* asset_manager_get(AssetType type, i64 id);

void asset_manager_destroy();

/* void asset_manager_load_asset_file(AssetType type, const char* name); */
/* void asset_manager_unload_asset(Asset asset); */


#endif // ASSET_MANAGER_H
