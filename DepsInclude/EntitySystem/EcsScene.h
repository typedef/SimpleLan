#ifndef ECS_SCENE_H
#define ECS_SCENE_H

#include <Utils/Types.h>

typedef struct EcsWorld EcsWorld;
typedef struct CameraComponent CameraComponent;
typedef struct AssetManager AssetManager;

typedef struct ViewportSettings
{
    f32 AspectRatio;
} ViewportSettings;

void scene_create();
void scene_save();
void scene_load();
EcsWorld* scene_get_world();
void* scene_get_selected_entity();
void scene_set_selected_entity(void* selectedEntity);
CameraComponent* scene_get_main_camera();
void scene_update_pointer_main_camera();
u32 scene_get_skybox();
void scene_update();
void scene_destroy();

#endif // ECS_SCENE_H
