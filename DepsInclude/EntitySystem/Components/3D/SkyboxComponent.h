#ifndef SKYBOX_COMPONENT_H
#define SKYBOX_COMPONENT_H

#include <Utils/Types.h>

typedef struct SkyboxComponent
{
    u32 Cubemap;
    char* Path;
} SkyboxComponent;

SkyboxComponent skybox_component_new(const char* path);

#endif // SKYBOX_COMPONENT_H
