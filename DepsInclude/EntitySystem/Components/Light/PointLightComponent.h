#ifndef POINT_LIGHT_COMPONENT_H
#define POINT_LIGHT_COMPONENT_H

#include "BaseLight.h"

typedef struct PointLightComponent
{
    BaseLight Base;
    v3 Color;
    v3 Position;
    f32 Constant;
    f32 Linear;
    f32 Quadratic;
} PointLightComponent;

#endif
