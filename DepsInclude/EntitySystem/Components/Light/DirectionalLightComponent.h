#ifndef DIRECTIONAL_LIGHT_COMPONENT_H
#define DIRECTIONAL_LIGHT_COMPONENT_H

#include "BaseLight.h"

typedef struct DirectionalLightComponent
{
    BaseLight Base;
    v3 Color;
    v3 Direction;
} DirectionalLightComponent;

#endif
