#ifndef ALL_COMPONENTS_H
#define ALL_COMPONENTS_H

#include "Base/TransformComponent.h"
#include "Base/CameraComponent.h"
#include "2D/SpriteComponent.h"
#include "3D/StaticModelComponent.h"
#include "3D/SkyboxComponent.h"

#endif
