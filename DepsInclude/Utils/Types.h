/*

		 #####################################
		 #####################################
			       Types.h
		 #####################################
		 #####################################

*/
#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

#if !defined(_STDIO_H)
int printf(const char * format, ... );
#endif //_STDIO_H

#if !defined(RED) || !defined(GREEN)
#define RED(x) "\x1B[31m"x"\033[0m"
#define GREEN(x) "\x1B[32m"x"\033[0m"
#endif // RED GREEN

#define BASE_ASSET_PATH "assets"
#define asset(path) BASE_ASSET_PATH path
#define asset_shader(shader) asset("/shaders/") shader
#define asset_texture(texture) asset("/textures/") texture
#define asset_cubemap(cubemap) asset("/cubemaps/") cubemap
#define asset_font(font) asset("/fonts/") font
#define asset_model(model) asset("/models/") model
#define asset_sound(sound) asset("/sounds/") sound
#define asset_scene(scene) asset("/scenes/") scene
#define asset_db(db) asset("/databases/") db
#define asset_localization(local) asset("/localization/") local

#define DEBUG_WO_FORCE_INLINE 0
#if DEBUG_WO_FORCE_INLINE == 1
#warning "Slow mode enabled!\n #define DEBUG_WO_FORCE_INLINE 1"
#endif

#if defined(WIN32) || defined(_WIN32) || DEBUG_WO_FORCE_INLINE
 #define force_inline static
#if defined(WINDOWS_PLATFORM) && !defined(DEBUG_WO_FORCE_INLINE)
   #define WINDOWS_PLATFORM
 #endif
#else
 #define force_inline static inline __attribute((always_inline))
 #ifndef LINUX_PLATFORM
  #define LINUX_PLATFORM
 #endif
#endif


#if !defined(vassert)

#if defined(ENGINE_DEBUG)
  #define vassert(a)							\
    ({									\
	if (!(a))							\
	{								\
	    printf(RED("[ASSERT]")" %s:%d condition: "GREEN("%s")" is false!\n", __FILE__, __LINE__, #a); \
	    *((i32*)((void*)0))	= 0;					\
	}								\
    })
  #define vassert_not_null(ptr) vassert(ptr != NULL)
  #define vassert_break() vassert(0 && "We shouldn't get here!!!")
  #define vassert_null_offset(ptr) vassert(!IS_NULL_OFFSET(ptr))
#else // ENGINE_DEBUG
  #define vassert(a) ((void)0)
  #define vassert_not_null(ptr) ((void)0)
  #define vassert_break() ((void)0)
  #define vassert_null_offset(ptr) ((void)0)
#endif // ENGINE_RELEASE

#endif // vassert

#if !defined(vguard)
#define vguard(ptr)							\
    ({									\
	if (!(ptr))							\
	{								\
	    printf(RED("[ASSERT]")" %s:%d condition: "GREEN("%s")" is false!\n", __FILE__, __LINE__, #ptr); \
	    *((i32*)((void*)0))	= 0;					\
	}								\
    })
#define vguard_not_null(ptr)			\
    ({						\
	vguard(ptr != NULL);			\
    })
#define vguard_null(ptr)			\
    ({						\
	vguard(ptr == NULL);			\
    })
#define vguard_legacy(func)				\
    ({							\
	vguard(0 && #func " - legacy function!");	\
    })

#endif // vguard

#define IfNullThen(x, t)			\
    ({						\
	__typeof__((x)) xv = (x);		\
	xv != NULL ? xv : (t);			\
    })

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float f32;
typedef double f64;
typedef u64 size_t;
#define i8(x)  ((i8)(x))
#define i16(x) ((i16)(x))
#define i32(x) ((i32)(x))
#define i32_ptr(x) ((i32*)(x))
#define i32_ptr_assign(x) *((i32*) (x))
#define i32_abs(x) ({ i32 temp = (x); temp < 0 ? -temp : temp; })
#define i64(x) ((i64)(x))
#define u8(x)  ((u8)(x))
#define u16(x) ((u16)(x))
#define u32(x) ((u32)(x))
#define u64(x) ((u64)(x))
#define f32(x) ((f32)(x))
#define f32_abs(x)				\
    ({						\
	union {f32 a; u32 b; } r;		\
	r.a = x;				\
	r.b &= 0x7fffffff;			\
	r.a;					\
    })
#define f64(x) ((f64)(x))

#define sing_equal(x, y)			\
    ({						\
	__typeof__(x) mulv = (x * y);		\
	(mulv >= 0) ? 1 : 0;			\
    })

#if !defined(false)
#define false 0
#endif // false
#if !defined(true)
#define true 1
#endif // true

#include <wchar.h>
typedef wchar_t wchar;
typedef struct WideString
{
    size_t Length;
    wchar* Buffer;
} WideString;

#define void_to_i32(ptr) ((ptr) ? (*((i32*)ptr)) : ({assert(0 && "Wrong argument void_to_i32!"); 0; }))
#define void_to_f32(ptr) (*((f32*)ptr))
#define void_to_string(ptr) ((const char*)ptr)
#define i32_to_void_ptr(i32v) ((void*)(&(i32v)))

#define I8_MAX 127
#define I16_MAX 32767
#define I32_MAX 2147483647
#define I32_MAX_HALF 1123241323
#define I64_MAX (9223372036854775807LL)
#define U8_MAX  255
#define U16_MAX 65535
#define U32_MAX 4294967295
#define U64_MAX (18446744073709551615ULL) /*18 446 744 073 709 551 615*/
#define F32_MAX (340282346638528859811704183484516925440.000000)

#ifndef NULL
#define NULL ((void*)0)
#endif

force_inline size_t
ptr_diff(void* ptr1, void* ptr2)
{
    size_t length = ((size_t)ptr2) - ((size_t)ptr1);
    return length;
}

typedef enum ResultType
{
    ResultType_Error = 0,
    ResultType_Success = 1,
    ResultType_Warning = 2
} ResultType;

#define EMPTY(type) (type){0}

//NOTE(typedef): we using gcc ext here for now
#define Align(byte) __attribute__((aligned(byte)))
#define TYPE_REVERSE(x) (x = !(x))
#define OffsetOf(Type, Field) ( (size_t) (&(((Type*)0)->Field)) )
#define AlignmentOf(Type) ((u64)&(((struct{char c; Type i;}*)0)->i))
#define AlignOf(Type) ((u64)&(((struct{char c; Type i;}*)0)->i))
#define ARRAY_LENGTH(x) (sizeof(x) / sizeof(x[0]))
#define ARRAY_COUNT(x) (sizeof(x) / sizeof(x[0]))
#define TO_STRING(x) #x
#define ToString(x) #x
#define FMOD(x, v) (f32) (((i32)(x)) % v + ((x) - (i32)(x)))
#define KB(x) ((i64)1024 * (i64)x)
#define MB(x) ((i64)1024 * KB(x))
#define GB(x) ((i64)1024 * MB(x))
#define I32T(t, h) t##h
#define I32M(m, t, h) m##t##h
#define I32B(b, m, t, h) b##m##t##h
#define TOKB(x) (((f64) x) / 1024)
#define TOMB(x) (((f64) TOKB(x)) / 1024)
#define TOGB(x) (((f64) TOMB(x)) / 1024)
#define ABS(x)					\
    ({						\
	((x) > 0) ? (x) : -(x);			\
    })

#define nullptr ((void*) 0)

#define K(x) (1000 * (x))

#define MAX(x, y)				\
    ({						\
	__typeof__(x) l = x;			\
	__typeof__(y) r = y;			\
	(l > r) ? l : r;			\
    })
#define MIN(x, y)				\
    ({						\
	__typeof__(x) l = x;			\
	__typeof__(y) r = y;			\
	(l > r) ? r : l;			\
    })
#define MINMAX(x, min, max)				\
    ({							\
	__typeof__(x) l = x;				\
	__typeof__(min) mn = min;			\
	__typeof__(max) mx = max;			\
	((l > mx) ? mx : (l < mn ? mn : l));		\
    })
#define SWAP(x, y)				\
    ({						\
	__typeof__(x) temp = x;			\
	x = y;					\
	y = temp;				\
    })

#define Max(x,y) MAX(x,y)
#define Min(x,y) MIN(x,y)
#define MinMax(x, min, max) MINMAX(x, min, max)
#define MinMaxV2(x, v2v) MINMAX(x, v2v.Min, v2v.Max)
#define Swap(x,y) SWAP(x,y)

typedef struct R
{
    u8 R;
} R;
typedef struct RG
{
    u8 R;
    u8 G;
} RG;
typedef struct RGB //((r << 16) | (g << 8) | (b))
{
    u8 R;
    u8 G;
    u8 B;
} RGB;

typedef struct RGBA //((r << 24) | (g << 16) | (b << 8) | (a))
{
    u8 R;
    u8 G;
    u8 B;
    u8 A;
} RGBA;
#define R(r) ((u32) (((RGBA*) (r))->R))
#define G(g) ((u32) (((RGBA*) (g))->G))
#define B(b) ((u32) (((RGBA*) (b))->B))
#define A(a) ((u32) (((RGBA*) (a))->A))
#define RGBA(r, g, b, a) ((a << 24) | (b << 16) | (g << 8) | (r))

#if  !defined(IS_NULL_OFFSET)
#define IS_NULL_OFFSET(ptr)						\
    ({									\
	vassert(sizeof(ptr) == 8 && "IS_NULL_OFFSET takes pointer as parameter!"); \
	u64 address = (u64)ptr;						\
	address < 10000 ? 1 : 0;					\
    })
#endif

#define DO_ONES(func) { static i8 flag = 1; if (flag) { flag = 0; func; } }
#define DO_ONES3(func, msg, out) { static i8 flag = 1; if (flag) { flag = 0; func(msg, out); } }

#define DO_MANY_TIME(action, time) {		\
	static i32 count = 0;			\
	if (count < time){			\
	    action;				\
	    ++count;				\
	}}

#define CALL_N(n)					\
    {							\
	static i32 start = 1;				\
	if (start > n)					\
	{						\
	    vassert(0 && "More then "#n" call");	\
	}						\
	++start;					\
    }

#define FlagSwitchDefine(enableFunction, disableFunction)	\
    ({								\
	static i32 flag = 0;					\
	if (!flag)						\
	{							\
	    flag = 1;						\
	    enableFunction;					\
	}							\
	else							\
	{							\
	    flag = 0;						\
	    disableFunction;					\
	}							\
    })

#define StructEquals(s1, s2)					\
    ({								\
	i32 result = 0;						\
	size_t size1 = sizeof((s1));				\
	size_t size2 = sizeof((s2));				\
								\
	if (size1 == size2)					\
	{							\
	    result = memcmp(&(s1), &(s2), size1) == 0 ? 1 : 0;	\
	}							\
								\
	result;							\
    })

typedef f32 v3a[3];
typedef f32 v4a[4];
typedef v3a m3a[3];
typedef v4a m4a[4];

typedef struct v2
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	};
	struct
	{
	    f32 Width;
	    f32 Height;
	};
	struct
	{
	    f32 Min;
	    f32 Max;
	};

	f32 V[2];
    };
} v2;
typedef struct v2i
{
    union
    {
	struct
	{
	    i32 X;
	    i32 Y;
	};
	struct
	{
	    i32 Width;
	    i32 Height;
	};

	i32 V[2];
    };
} v2i;

typedef struct v3
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	    f32 Z;
	};
	struct
	{
	    f32 R;
	    f32 G;
	    f32 B;
	};
	struct
	{
	    f32 Hue;
	    f32 Saturation;
	    f32 Value;
	};
	struct
	{
	    f32 Pitch;
	    f32 Yaw;
	    f32 Roll;
	};

	v2 XY;
	f32 V[3];

    };
} v3;

typedef struct v4
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	    f32 Z;
	    f32 W;
	};
	struct
	{
	    f32 R;
	    f32 G;
	    f32 B;
	    f32 A;
	};
	f32 V[4];
    };
} v4;

typedef struct v4i
{
    union
    {
	struct
	{
	    i32 X;
	    i32 Y;
	    i32 Z;
	    i32 W;
	};

	struct
	{
	    i32 R;
	    i32 G;
	    i32 B;
	    i32 A;
	};

	i32 V[4];
    };

} v4i;

typedef struct m3
{
    union
    {
	struct
	{
	    f32 M00;
	    f32 M01;
	    f32 M02;

	    f32 M10;
	    f32 M11;
	    f32 M12;

	    f32 M20;
	    f32 M21;
	    f32 M22;
	};
	v3 V[3];
	m3a M;
    };
} m3;

typedef struct m4
{
    union
    {
	struct
	{
	    f32 M00;
	    f32 M01;
	    f32 M02;
	    f32 M03;

	    f32 M10;
	    f32 M11;
	    f32 M12;
	    f32 M13;

	    f32 M20;
	    f32 M21;
	    f32 M22;
	    f32 M23;

	    f32 M30;
	    f32 M31;
	    f32 M32;
	    f32 M33;
	};

	v4 V[4];
	m4a M;
    };
} m4;


typedef struct quat
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	    f32 Z;
	    f32 W;
	};

	v4 V4;

	f32 V[4];
    };
} quat;

typedef struct Aabb
{
    v3 Min;
    v3 Max;
} Aabb;

typedef struct Arena
{
    i64 Id;
    size_t Offset;
    size_t Size;
    i32 Line;
    const char* File;
    void* Data;
} Arena;

#endif // Types.h
