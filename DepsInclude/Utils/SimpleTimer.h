#ifndef SIMPLE_TIMER_H
#define SIMPLE_TIMER_H

typedef enum SimpleTimerType
{
    SimpleTimerType_None = 0,

    /*
      DOCS(typedef): Global level timer it responsible:
      * How long application run
      * When it was started
    */
    SimpleTimerType_Global,

    /*
      DOCS(typedef): Local level timer it responsible:
      * Saves wait duration
      * Count how many times gone from start
    */
    SimpleTimerType_Local,

} SimpleTimerType;

typedef struct SimpleTimer
{
    SimpleTimerType Type;

    // DOCS(): _Global
    f32 SecondsTotal;

    // DOCS(): _Local
    f32 DurationTimeSeconds;

} SimpleTimer;

SimpleTimer simple_timer_create()
{

}

/*

  API I WANT:

  Measurements:
  Seconds: 3.402823466 E + 38
  Minutes: 3.402823466 E + 36
  Hours:   3.402823466 E + 34

  Ratio for (float ms) passed to update function

  do_some_action() call every 0.9f seconds


  SimpleTimerHub.AddAnotherTimer();

  // Updating it from App
  SimpleTimerHub.ForEach(simple_timer_update);



*/

/*
  Application/Miliseconds related timer
  * How long system runs
  * Current second progress (891 ms from prev update)
  *

  timer_call_every_n(&smplTimer, secondRatio=0.4f, call_back_function_delegate);

  better name:
  simple_timer_

*/

#endif // SIMPLE_TIMER_H
